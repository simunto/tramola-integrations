package com.simunto.tramola.datapusher;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import okhttp3.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;

import static java.util.stream.Collectors.groupingBy;

/**
 * @author mrieser / Simunto
 */
public class Run {

    public static String OUTPUT_INFO_FILE = "tramola_output.json";

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static private final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    static private final MediaType MEDIA_TYPE_BINARY = MediaType.parse("application/octet-stream");

    private long id = -1;
    private final String name;
    private final long timestamp;
    private final List<Attribute> attributes = new ArrayList<>(5);

    private Communicator communicator = null;

    Run(long id, String name, long timestamp, List<Attribute> attributes) {
        this.id = id;
        this.name = name;
        this.timestamp = timestamp;
        this.attributes.addAll(attributes);
    }

    /**
     * Creates empty tramola output file so the runner knows, nothing needs to be uploaded.
     */
    public static void createOutputFile(File outputDir) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(outputDir, OUTPUT_INFO_FILE)))) {
            writer.write("[]");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static RunBuilder build() {
        return new RunBuilder();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return this.name;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public List<Attribute> getAttributes() {
        return Collections.unmodifiableList(this.attributes);
    }

    public Serie addSerie(String serieName, Serie.SerieType type) {
        Serie serie = new Serie(this, serieName, type);
        this.communicator.registerSerie(serie);
        return serie;
    }

    void addReading(Serie series, Reading reading) {
        reading.setSeries(series);
        this.communicator.addReading(reading);
    }

    public void addFile(File file) {
        this.communicator.addFile(file);
    }

    public void addFile(String filename, byte[] data) {
        this.communicator.addFile(filename, data);
    }

    /**
     * Recursively uploads file within a directory.
     */
    public void uploadFilesInDirectory(Matcher matcher, File directory) {
        File[] outputFiles = directory.listFiles();
        if (outputFiles != null) {
            for (File file : outputFiles) {
                if (file.isDirectory()) {
                    uploadFilesInDirectory(matcher, file);
                }
                if (file.isFile()) {
                    String filename = file.getName().toLowerCase(Locale.ROOT);
                    matcher.reset(filename);
                    if (matcher.matches()) {
                        addFile(file);
                    }
                }
            }
        }
    }

    public void importCSV(String serieName, File file) throws IOException {
        importCSV(serieName, file, null);
    }

    public void importCSV(String serieName, File file, String iterationColumnName) throws IOException {
        try (CsvReader reader = new CsvReader(file.getAbsolutePath(), StandardCharsets.UTF_8)) {
            importCsv(serieName, reader, iterationColumnName);
        }
    }

    public void importTabSeparated(String serieName, File file) throws IOException {
        importTabSeparated(serieName, file, null);
    }

    public void importTabSeparated(String serieName, File file, String iterationColumnName) throws IOException {
        try (CsvReader reader = new CsvReader(file.getAbsolutePath(), StandardCharsets.UTF_8)) {
            reader.setFieldSeparator('\t');
            importCsv(serieName, reader, iterationColumnName);
        }
    }

    private void importCsv(String serieName, CsvReader reader, String iterationColumnName) throws IOException {
        String[] header = reader.readHeader();

        Serie serie = new Serie(this, serieName, Serie.SerieType.STRING);
        this.communicator.registerSerie(serie);

        String[] row = null;
        while ((row = reader.readRecord()) != null) {
            Reading.ReadingBuilder rb = Reading.build();
            int cols = Math.min(header.length, row.length);
            for (int i = 0; i < cols; i++) {
                String key = header[i];
                String val = row[i];
                if (!val.isEmpty()) {
                    if (key.equals(iterationColumnName)) {
                        try {
                            int iter = Integer.parseInt(val.trim());
                            rb.iteration(iter);
                        } catch (NumberFormatException ignored) {
                        }
                    } else {
                        try {
                            double v = Double.parseDouble(val.trim());
                            rb.value(key, v);
                        } catch (NumberFormatException e) {
                            rb.value(key, val);
                        }
                    }
                }
            }
            Reading r = rb.get();
            if (!r.getValues().isEmpty()) {
                serie.addReading(r);
            }
        }
    }

    public void addEvent(String type, String data) {
        this.communicator.addEvent(type, data);
    }

    public void close() {
        if (this.communicator != null) {
            this.communicator.close();
        }
    }

    void register(String hostUrl, String apikey, String password, String group) throws IllegalStateException {
        if (this.communicator != null) {
            throw new IllegalStateException("Run already registered.");
        }
        this.communicator = new Communicator(hostUrl, apikey, password, group);
        this.communicator.registerRun();
    }

    public static class RunBuilder {
        private long id = -1;
        private String name = "Unnamed Run";
        private long timestamp = Timer.currentTimeMillis();
        private List<Attribute> attributes = new ArrayList<>(5);

        public RunBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public RunBuilder withId(long id) {
            this.id = id;
            return this;
        }

        public RunBuilder withAttribute(String name, double value) {
            this.attributes.add(new DoubleAttribute(name, value));
            return this;
        }

        public RunBuilder withAttribute(String name, long value) {
            this.attributes.add(new LongAttribute(name, value));
            return this;
        }

        public RunBuilder withAttribute(String name, int value) {
            this.attributes.add(new IntAttribute(name, value));
            return this;
        }

        public RunBuilder withAttribute(String name, boolean value) {
            this.attributes.add(new BooleanAttribute(name, value));
            return this;
        }

        public RunBuilder withAttribute(String name, String value) {
            this.attributes.add(new StringAttribute(name, value));
            return this;
        }

        public Run get() {
            return new Run(this.id, this.name, this.timestamp, this.attributes);
        }
    }

    private class Communicator {

        private final String hostUrl;
        private final String apikey;
        private final String password;
        private final String group;

        private Thread thread = null;
        private ArrayBlockingQueue<Reading> readings = null;
        private AtomicBoolean closeRequested = new AtomicBoolean(false);
        private AtomicBoolean flushRequested = new AtomicBoolean(false);
        private CyclicBarrier flushBarrier = new CyclicBarrier(2);

        private OkHttpClient http = new OkHttpClient();

        Communicator(String hostUrl, String apikey, String password, String group) {
            this.hostUrl = hostUrl;
            this.apikey = apikey;
            this.password = password;
            this.group = group;
        }

        void registerRun() {
            // see if run id is already set from config
            long runId = Run.this.id;
            if (Run.this.id == -1) {
                // alternatively check system environment for run id
                String runIdFromEnv = System.getenv("tramola.runId");
                if (runIdFromEnv != null) {
                    runId = Long.parseLong(runIdFromEnv);
                }
            }

            if (runId != -1) {
                updateRun(runId);
                return;
            }

            ObjectNode node = MAPPER.createObjectNode();
            node.put("type", "run");
            node.put("name", Run.this.name);
            node.put("timestamp", Run.this.timestamp);
            if (this.group != null) {
                node.put("groupName", this.group);
            }
            ObjectNode attributes = node.putObject("attributes");
            for (Attribute a : Run.this.attributes) {
                ObjectNode attr = MAPPER.createObjectNode();
                attr.put("name", a.name);
                if (a instanceof BooleanAttribute) {
                    attributes.put(a.name, ((BooleanAttribute) a).value);
                }
                if (a instanceof DoubleAttribute) {
                    attributes.put(a.name, ((DoubleAttribute) a).value);
                }
                if (a instanceof IntAttribute) {
                    attributes.put(a.name, ((IntAttribute) a).value);
                }
                if (a instanceof LongAttribute) {
                    attributes.put(a.name, ((LongAttribute) a).value);
                }
                if (a instanceof StringAttribute) {
                    attributes.put(a.name, ((StringAttribute) a).value);
                }
            }

            String body;
            try {
                body = MAPPER.writeValueAsString(node);
            } catch (JsonProcessingException e) {
                throw new UncheckedIOException(e);
            }
            Request request = new Request.Builder()
                    .url(this.hostUrl + "/api/v1/datasets/datasets")
                    .addHeader("Authorization", "tramola-apikey " + this.apikey)
                    .post(RequestBody.create(body, MEDIA_TYPE_JSON))
                    .build();
            try (Response response = this.http.newCall(request).execute()) {
                String result = response.body().string();

                JsonNode resultNode = MAPPER.reader().readTree(result);

                Run.this.id = resultNode.get("id").asLong();
            } catch (IOException e) {
                Run.this.id = -1;
                throw new UncheckedIOException(e.getMessage(), e);
            } finally {
                startThreads();
            }
        }

        private void updateRun(long runId) {
            ObjectNode node = MAPPER.createObjectNode();
            ObjectNode attributes = node.putObject("attributes");
            for (Attribute a : Run.this.attributes) {
                ObjectNode attr = MAPPER.createObjectNode();
                attr.put("name", a.name);
                if (a instanceof BooleanAttribute) {
                    attributes.put(a.name, ((BooleanAttribute) a).value);
                }
                if (a instanceof DoubleAttribute) {
                    attributes.put(a.name, ((DoubleAttribute) a).value);
                }
                if (a instanceof IntAttribute) {
                    attributes.put(a.name, ((IntAttribute) a).value);
                }
                if (a instanceof LongAttribute) {
                    attributes.put(a.name, ((LongAttribute) a).value);
                }
                if (a instanceof StringAttribute) {
                    attributes.put(a.name, ((StringAttribute) a).value);
                }
            }
            try {
                String body = MAPPER.writeValueAsString(node);

                Request request = new Request.Builder()
                        .url(this.hostUrl + "/api/v1/datasets/datasets/" + runId)
                        .addHeader("Authorization", "tramola-apikey " + this.apikey)
                        .put(RequestBody.create(body, MEDIA_TYPE_JSON))
                        .build();
                try (Response response = this.http.newCall(request).execute()) {
                    String result = response.body().string();
                    JsonNode resultNode = MAPPER.reader().readTree(result);
                    Run.this.id = resultNode.get("id").asLong();
                }
            } catch (IOException e) {
                Run.this.id = -1;
                throw new UncheckedIOException(e.getMessage(), e);
            } finally {
                startThreads();
            }
        }

        void registerSerie(Serie serie) {
            if (Run.this.id == -1) {
                throw new IllegalStateException("Run is not yet registered.");
            }
            if (serie.getId() != -1) {
                throw new IllegalStateException("Serie is already registered.");
            }

            ArrayNode fields = MAPPER.createArrayNode();
            fields.add(MAPPER.createObjectNode().put("name", "key").put("type", "STRING"));
            fields.add(MAPPER.createObjectNode().put("name", "time").put("type", "LONG"));
            fields.add(MAPPER.createObjectNode().put("name", "value").put("type", serie.getType()));
            fields.add(MAPPER.createObjectNode().put("name", "iteration").put("type", "LONG"));

            ObjectNode node = MAPPER.createObjectNode();
            node.put("name", serie.getName());
            node.put("fields", fields);
            try {
                String body = MAPPER.writeValueAsString(node);

                Request request = new Request.Builder()
                        .url(this.hostUrl + "/api/v1/datasets/datasets/" + Run.this.id + "/tables")
                        .addHeader("Authorization", "tramola-apikey " + this.apikey)
                        .post(RequestBody.create(body, MEDIA_TYPE_JSON))
                        .build();

                try (Response response = this.http.newCall(request).execute()) {
                    String result = response.body().string();
                    JsonNode resultNode = MAPPER.reader().readTree(result);
                    serie.setId(resultNode.get("id").asLong());
                    serie.setName(resultNode.get("name").asText());
                }
            } catch (IOException e) {
                throw new UncheckedIOException(e.getMessage(), e);
            }
        }

        private void startThreads() {
            this.readings = new ArrayBlockingQueue<>(10000);

            this.thread = new Thread(this::runSubmitReadings, "readings-" + Run.this.id);
            this.thread.setDaemon(true);
            this.thread.start();
        }

        void addReading(Reading reading) {
            try {
                this.readings.put(reading);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void runSubmitReadings() {
            ArrayList<Reading> list = new ArrayList<>();
            boolean flushRequest = false;
            while (!this.closeRequested.get()) {
                flushRequest = this.flushRequested.getAndSet(false);
                try {
                    submitReadings(list);
                } catch (UncheckedIOException e) {
                    e.printStackTrace();
                }
                if (flushRequest) {
                    try {
                        this.flushBarrier.await();
                    } catch (BrokenBarrierException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    Thread.sleep(5_000);
                } catch (InterruptedException e) {
                    if (!this.closeRequested.get() && !this.flushRequested.get()) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                submitReadings(list);
            } catch (UncheckedIOException e) {
                e.printStackTrace();
            }
        }

        private void submitReadings(ArrayList<Reading> list) {
            this.readings.drainTo(list);
            Map<String, List<Reading>> seriesReadings = list.stream().collect(groupingBy(reading -> reading.getSeries().getName()));
            list.clear();
            for (String seriesName : seriesReadings.keySet()) {
                List<Reading> seriesList = seriesReadings.get(seriesName);
                if (!seriesList.isEmpty()) {
                    ArrayNode jsonList = MAPPER.createArrayNode();

                    for (Reading r : seriesList) {
                        for (Attribute a : r.getValues()) {
                            ObjectNode node = MAPPER.createObjectNode();
                            node.put("time", r.getTimestamp());
                            if (r.getIteration() != null) {
                                node.put("iteration", r.getIteration().intValue());
                            }
                            ObjectNode attr = MAPPER.createObjectNode();
                            node.put("key", a.name);
                            if (a instanceof BooleanAttribute) {
                                node.put("value", ((BooleanAttribute) a).value);
                            }
                            if (a instanceof DoubleAttribute) {
                                node.put("value", ((DoubleAttribute) a).value);
                            }
                            if (a instanceof IntAttribute) {
                                node.put("value", ((IntAttribute) a).value);
                            }
                            if (a instanceof LongAttribute) {
                                node.put("value", ((LongAttribute) a).value);
                            }
                            if (a instanceof StringAttribute) {
                                node.put("value", ((StringAttribute) a).value);
                            }
                            jsonList.add(node);
                        }
                    }

                    try {
                        String body = MAPPER.writeValueAsString(jsonList);

                        Request request = new Request.Builder()
                                .url(this.hostUrl + "/api/v1/datasets/datasets/" + Run.this.id + "/tables/" + seriesName)
                                .addHeader("Authorization", "tramola-apikey " + this.apikey)
                                .patch(RequestBody.create(body, MEDIA_TYPE_JSON))
                                .build();
                        try (Response response = this.http.newCall(request).execute(); ResponseBody resBody = response.body()) {
                        }
                    } catch (IOException e) {
                        this.readings.addAll(seriesList);
                        throw new UncheckedIOException(e.getMessage(), e);
                    }
                }
            }

        }

        void addFile(File file) {
            try {
                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("file", file.getName(),
                                RequestBody.create(file, MEDIA_TYPE_BINARY))
                        .build();

                Request request = new Request.Builder()
                        .addHeader("Authorization", "tramola-apikey " + this.apikey)
                        .url(this.hostUrl + "/api/v1/datasets/files")
                        .post(requestBody)
                        .build();
                try (Response response = this.http.newCall(request).execute()) {
                    String result = response.body().string();

                    JsonNode resultNode = MAPPER.reader().readTree(result);
                    if (resultNode.get("files").isArray()) {
                        long fileId = resultNode.get("files").get(0).get("id").asLong();
                        // "/datasets/:dId/files/:fId"

                        Request request1 = new Request.Builder()
                                .url(this.hostUrl + "/api/v1/datasets/datasets/" + Run.this.id + "/files/" + fileId)
                                .addHeader("Authorization", "tramola-apikey " + this.apikey)
                                .post(RequestBody.create(new byte[0], MEDIA_TYPE_BINARY))
                                .build();
                        try (Response response1 = this.http.newCall(request1).execute(); ResponseBody resBody1 = response1.body()) {
                        }
                    }
                }
            } catch (IOException e) {
                throw new UncheckedIOException(e.getMessage(), e);
            }
        }

        void addFile(String filename, byte[] data) {
            try {
                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("file", filename,
                                RequestBody.create(data, MEDIA_TYPE_BINARY))
                        .build();

                Request request = new Request.Builder()
                        .addHeader("Authorization", "tramola-apikey " + this.apikey)
                        .url(this.hostUrl + "/api/v1/datasets/files")
                        .post(requestBody)
                        .build();
                try (Response response = this.http.newCall(request).execute()) {
                    String result = response.body().string();

                    JsonNode resultNode = MAPPER.reader().readTree(result);
                    if (resultNode.get("files").isArray()) {
                        long fileId = resultNode.get("files").get(0).get("id").asLong();
                        // "/datasets/:dId/files/:fId"

                        Request request1 = new Request.Builder()
                                .url(this.hostUrl + "/api/v1/datasets/datasets/" + Run.this.id + "/files/" + fileId)
                                .addHeader("Authorization", "tramola-apikey " + this.apikey)
                                .post(RequestBody.create(new byte[0], MEDIA_TYPE_BINARY))
                                .build();
                        try (Response response1 = this.http.newCall(request1).execute(); ResponseBody resBody1 = response1.body()) {
                        }
                    }
                }
            } catch (IOException e) {
                throw new UncheckedIOException(e.getMessage(), e);
            }
        }

        void addEvent(String type, String data) {
            try {
                flushReadings();
                ObjectNode payload = MAPPER.createObjectNode();
                payload.put("timestamp", Timer.currentTimeMillis());
                payload.put("type", type);
                payload.put("data", data == null ? "" : data);
                String body = MAPPER.writeValueAsString(payload);

                Request request = new Request.Builder()
                        .url(this.hostUrl + "/api/v1/datasets/datasets/" + Run.this.id + "/events")
                        .addHeader("Authorization", "tramola-apikey " + this.apikey)
                        .post(RequestBody.create(body, MEDIA_TYPE_JSON))
                        .build();
                try (Response response = this.http.newCall(request).execute(); ResponseBody resBody = response.body()) {
                }

            } catch (IOException e) {
                throw new UncheckedIOException(e.getMessage(), e);
            }
        }

        private synchronized void flushReadings() {
            this.flushRequested.set(true);
            this.thread.interrupt();
            try {
                this.flushBarrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }

        void close() {
            this.closeRequested.set(true);
            this.thread.interrupt();
            try {
                this.thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
