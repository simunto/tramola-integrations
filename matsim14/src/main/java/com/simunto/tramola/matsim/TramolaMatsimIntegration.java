package com.simunto.tramola.matsim;

import com.simunto.tramola.datapusher.Run;
import com.simunto.tramola.datapusher.TramolaClient;
import com.simunto.tramola.datapusher.datasources.MemoryObserver;
import org.apache.log4j.Logger;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.gbl.Gbl;

import javax.inject.Singleton;

/**
 * @author mrieser / Simunto
 */
public class TramolaMatsimIntegration extends AbstractModule {

	private static final Logger log = Logger.getLogger(TramolaControlerListener.class);
	private boolean isEnabled;
	private TramolaClient tramolaClient = null;
	private TramolaIntegrationConfigGroup tramolaConfig = null;

	public TramolaMatsimIntegration(Config config) {
		super(config);
	}

	public void init() {
		this.tramolaConfig = ConfigUtils.addOrGetModule(super.getConfig(), TramolaIntegrationConfigGroup.class);

		String magHostUrl = this.tramolaConfig.getUrl();
		String magApiKey = this.tramolaConfig.getApiKey();
		String magPassword = this.tramolaConfig.getApiPassword();
		long magDatasetId = this.tramolaConfig.getDatasetId();
//		boolean debug = this.tramolaConfig.isDebug();
		this.tramolaClient = new TramolaClient(magHostUrl, magApiKey, magPassword);
		this.tramolaClient.setGroup(this.tramolaConfig.getGroup());

		this.isEnabled = this.tramolaConfig.isEnabled();
		if (this.isEnabled) {
			String matsimRunId = super.getConfig().controler().getRunId();
			String crs = super.getConfig().global().getCoordinateSystem();
			if (matsimRunId == null || matsimRunId.isEmpty()) {
				matsimRunId = "Untitled Run";
			}
			Run run = Run.build()
					.withId(magDatasetId)
					.withName(matsimRunId)
					.withAttribute("maxMemory", Runtime.getRuntime().maxMemory())
					.withAttribute("java.version", System.getProperty("java.version"))
					.withAttribute("java.vm.vendor", System.getProperty("java.vm.vendor"))
					.withAttribute("java.vm.info", System.getProperty("java.vm.info"))
					.withAttribute("os.name", System.getProperty("os.name"))
					.withAttribute("os.version", System.getProperty("os.version"))
					.withAttribute("os.arch", System.getProperty("os.arch"))
					.withAttribute("availProcessors", Runtime.getRuntime().availableProcessors())
					.withAttribute("matsim.version", Gbl.getBuildInfoString())
					.withAttribute("__crs", crs)
					.get();
			try {
				this.tramolaClient.registerRun(run);
				log.info("Registered run at " + this.tramolaClient.getName());

				this.tramolaClient.getRegisteredRun().addEvent("runStarted", "");

				new MemoryObserver(this.tramolaClient).start(15);
			} catch (Exception e) {
				log.error("Could not register run at host", e);
				this.isEnabled = false;
			}

		}
	}

	@Override
	public void install() {
		if (this.tramolaClient == null) {
			init();
			// if the magClient already exists, all specific config groups will automatically be bound, so only bind it if we just created it
			this.bind(TramolaIntegrationConfigGroup.class).toInstance(this.tramolaConfig);
		}
		this.bind(TramolaClient.class).toInstance(this.tramolaClient);
		if (this.isEnabled) {
			this.bind(TramolaControlerListener.class).in(Singleton.class);
			this.addControlerListenerBinding().to(TramolaControlerListener.class);
		}
	}
}
