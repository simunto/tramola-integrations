package com.simunto.tramola.datapusher;

/**
 * @author mrieser
 */
class BooleanAttribute extends Attribute {

	final boolean value;

	BooleanAttribute(String name, boolean value) {
		super(name);
		this.value = value;
	}
}
