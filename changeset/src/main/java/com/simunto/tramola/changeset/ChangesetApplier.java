package com.simunto.tramola.changeset;

import com.google.common.collect.Sets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.network.Node;
import org.matsim.core.population.routes.NetworkRoute;
import org.matsim.core.population.routes.RouteUtils;
import org.matsim.core.utils.geometry.CoordinateTransformation;
import org.matsim.core.utils.misc.OptionalTime;
import org.matsim.pt.transitSchedule.TransitScheduleFactoryImpl;
import org.matsim.pt.transitSchedule.api.Departure;
import org.matsim.pt.transitSchedule.api.TransitLine;
import org.matsim.pt.transitSchedule.api.TransitRoute;
import org.matsim.pt.transitSchedule.api.TransitRouteStop;
import org.matsim.pt.transitSchedule.api.TransitSchedule;
import org.matsim.pt.transitSchedule.api.TransitScheduleFactory;
import org.matsim.pt.transitSchedule.api.TransitStopArea;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;
import org.matsim.utils.objectattributes.attributable.AttributesUtils;
import org.matsim.vehicles.Vehicle;
import org.matsim.vehicles.VehicleType;
import org.matsim.vehicles.VehicleUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import static com.simunto.tramola.changeset.Change.*;

/**
 * Class to apply {@link Changeset}s to a scenario.
 */
public class ChangesetApplier {

	private static final Logger log = LogManager.getLogger(ChangesetApplier.class);

	private final Scenario scenario;
	private final TransitSchedule schedule;
	private final Network network;
	private final CoordinateTransformation ct;

	public ChangesetApplier(Scenario scenario, CoordinateTransformation ct) {
		this.scenario = scenario;
		this.network = scenario.getNetwork();
		this.schedule = scenario.getTransitSchedule();
		this.ct = ct;
	}

	private static Map<Id<Link>, Set<TransitRoute>> mapRoutes(TransitSchedule schedule) {
		Map<Id<Link>, Set<TransitRoute>> map = new HashMap<>();
		for (TransitLine tl : schedule.getTransitLines().values()) {
			for (TransitRoute r : tl.getRoutes().values()) {

				map.computeIfAbsent(r.getRoute().getStartLinkId(), (k) -> new HashSet<>()).add(r);
				map.computeIfAbsent(r.getRoute().getEndLinkId(), (k) -> new HashSet<>()).add(r);

				for (Id<Link> linkId : r.getRoute().getLinkIds()) {
					map.computeIfAbsent(linkId, (k) -> new HashSet<>()).add(r);
				}
			}
		}
		return map;
	}

	public void applyChanges(List<Changeset> changesets) {
		for (Changeset changeset : changesets) {
			for (Change change : changeset.modifications) {
				applyChange(change);
			}
		}
		if (this.scenario.getTransitSchedule() != null && this.scenario.getTransitSchedule().getTransitLines().size() > 0) {
			new ScenarioFixer(this.scenario).fixTransitSchedule();
		}
	}

	private void applyChange(Change change) {
		switch (change.type) {
			// Network changes
			case AddNodePayload.TYPE -> applyChange((AddNodePayload) change.payload);
			case UpdateNodePayload.TYPE -> applyChange((UpdateNodePayload) change.payload);
			case DeleteNodePayload.TYPE -> applyChange((DeleteNodePayload) change.payload);
			case NetworkGroupPayload.TYPE -> applyChange((NetworkGroupPayload) change.payload);
			case AddLinkPayload.TYPE -> applyChange((AddLinkPayload) change.payload);
			case UpdateLinkPayload.TYPE -> applyChange((UpdateLinkPayload) change.payload);
			case DeleteLinkPayload.TYPE -> applyChange((DeleteLinkPayload) change.payload);
			// Transit Changes
			case AddTransitStopPayload.TYPE -> applyChange((AddTransitStopPayload) change.payload);
			case UpdateTransitStopPayload.TYPE -> applyChange((UpdateTransitStopPayload) change.payload);
			case DeleteTransitStopPayload.TYPE -> applyChange((DeleteTransitStopPayload) change.payload);
			case TransitStopGroupPayload.TYPE -> applyChange((TransitStopGroupPayload) change.payload);
			case AddTransitLinePayload.TYPE -> applyChange((AddTransitLinePayload) change.payload);
			case UpdateTransitLinePayload.TYPE -> applyChange((UpdateTransitLinePayload) change.payload);
			case DeleteTransitLinePayload.TYPE -> applyChange((DeleteTransitLinePayload) change.payload);
			case TransitLineGroupPayload.TYPE -> applyChange((TransitLineGroupPayload) change.payload);
			case AddTransitRoutePayload.TYPE -> applyChange((AddTransitRoutePayload) change.payload);
			case UpdateTransitRoutePayload.TYPE -> applyChange((UpdateTransitRoutePayload) change.payload);
			case DeleteTransitRoutePayload.TYPE -> applyChange((DeleteTransitRoutePayload) change.payload);
			case AddTransitRouteStopPayload.TYPE -> applyChange((AddTransitRouteStopPayload) change.payload);
			case UpdateTransitRouteStopPayload.TYPE -> applyChange((UpdateTransitRouteStopPayload) change.payload);
			case DeleteTransitRouteStopPayload.TYPE -> applyChange((DeleteTransitRouteStopPayload) change.payload);
			case UpdateTransitRouteDeparturesPayload.TYPE ->
				applyChange((UpdateTransitRouteDeparturesPayload) change.payload);
			case DeleteTransitNetworkRouteLinkPayload.TYPE -> applyChange((DeleteTransitNetworkRouteLinkPayload) change.payload);
			case AddTransitNetworkRouteLinkPayload.TYPE -> applyChange((AddTransitNetworkRouteLinkPayload) change.payload);
		}
	}

	private void applyChange(AddNodePayload change) {
		Coord coord = this.ct.transform(new Coord(change.lng, change.lat));
		Node node = network.getFactory().createNode(Id.createNodeId(change.id), coord);
		network.addNode(node);
	}

	private void applyChange(UpdateNodePayload change) {
		Node node = network.getNodes().get(Id.createNodeId(change.id));
		Coord coord = this.ct.transform(new Coord(change.lng, change.lat));
		node.setCoord(coord);
	}

	private void applyChange(DeleteNodePayload change) {
		network.removeNode(Id.createNodeId(change.id));
	}

	private void applyChange(NetworkGroupPayload change) {

		Set<RemovedLink> toDelete = new LinkedHashSet<>();
		List<AddLinkPayload> toAdd = new LinkedList<>();

		for (Change modification : change.modifications) {

			if (modification.payload instanceof DeleteLinkPayload delete) {
				Link l = network.getLinks().get(Id.createLinkId(delete.id));
				toDelete.add(new RemovedLink(l.getId(), l.getFromNode().getId(), l.getToNode().getId()));
			}

			if (modification.payload instanceof AddLinkPayload add)
				toAdd.add(add);

			// Apply the modification same as others
			applyChange(modification);
		}

		Map<Id<Link>, Set<TransitRoute>> linkToPt = mapRoutes(schedule);

		// try to adjust pt routes that have been affected by the changed links
		// this only for up-to two modified links in the sequence
		for (RemovedLink delete : toDelete) {

			List<Id<Link>> replace = toAdd.stream().filter(l -> l.fromId.equals(delete.fromNode.toString()) ||
					l.toId.equals(delete.toNode.toString()))
				.map(l -> Id.createLinkId(l.id))
				.toList();

			if (replace.isEmpty())
				continue;

			for (TransitStopFacility stop : this.schedule.getFacilities().values()) {
				if (stop.getLinkId().equals(delete.id)) {
					stop.setLinkId(replace.get(0));
				}
			}

			for (TransitRoute route : linkToPt.computeIfAbsent(delete.id, (k) -> new HashSet<>())) {

				Id<Link> startLinkId = route.getRoute().getStartLinkId();
				Id<Link> endLinkId = route.getRoute().getEndLinkId();
				List<Id<Link>> linkIds = new ArrayList<>(route.getRoute().getLinkIds());

				if (startLinkId.equals(delete.id)) {
					startLinkId = replace.get(0);
					linkIds.addAll(0, replace.subList(1, replace.size()));
				} else if (endLinkId.equals(delete.id)) {
					endLinkId = replace.get(replace.size() - 1);
					linkIds.addAll(replace.subList(0, replace.size() - 1));
				} else {
					int idx = linkIds.indexOf(delete.id);
					if (idx > -1) {
						linkIds.remove(idx);
						linkIds.addAll(idx, replace);
					}
				}

				route.getRoute().setLinkIds(startLinkId, linkIds, endLinkId);
			}
		}
	}

	private void applyChange(AddLinkPayload change) {

		Node fromNode = network.getNodes().get(Id.createNodeId(change.fromId));
		Node toNode = network.getNodes().get(Id.createNodeId(change.toId));

		Link link = network.getFactory().createLink(Id.createLinkId(change.id), fromNode, toNode);

		applyLinkAttributes(link, change.properties);

		network.addLink(link);
	}

	private void applyChange(UpdateLinkPayload change) {
		Link link = network.getLinks().get(Id.createLinkId(change.id));
		applyLinkAttributes(link, change.properties);
	}

	private static void applyLinkAttributes(Link link, Map<String, Object> properties) {

		for (Map.Entry<String, Object> e : properties.entrySet()) {
			Object v = e.getValue();

			switch (e.getKey()) {
				case "length" -> link.setLength(asDouble(v));
				case "capacity" -> link.setCapacity(asDouble(v));
				case "freespeed" -> link.setFreespeed(asDouble(v));
				case "lanes" -> link.setNumberOfLanes(asDouble(v));
				case "modes" -> {
					String[] modes = v.toString().split(",");
					link.setAllowedModes(Sets.newHashSet(modes));
				}
				default -> link.getAttributes().putAttribute(e.getKey(), v);
			}
		}
	}

	private static double asDouble(Object o) {
		if (o instanceof Number) {
			return ((Number) o).doubleValue();
		}
		try {
			return Double.parseDouble(o.toString());
		} catch (NumberFormatException e) {
			log.error("Cannot convert to double: " + o, e);
		}
		return Double.NaN;
	}

	private void applyChange(DeleteLinkPayload change) {
		network.removeLink(Id.createLinkId(change.id));
	}

	private void applyChange(AddTransitStopPayload change) {
		Coord coord = this.ct.transform(new Coord(change.stop.coordinates));
		TransitStopFacility facility = this.schedule.getFactory().createTransitStopFacility(
			Id.create(change.id, TransitStopFacility.class),
			coord,
			change.stop.isBlocking
		);

		if (change.stop.linkId != null)
			facility.setLinkId(Id.createLinkId(change.stop.linkId));

		if (change.stop.stopAreaId != null)
			facility.setStopAreaId(Id.create(change.stop.stopAreaId, TransitStopArea.class));

		facility.setName(change.stop.name);

		this.schedule.addStopFacility(facility);
	}

	private void applyChange(UpdateTransitStopPayload change) {
		Id<TransitStopFacility> stopId = Id.create(change.id, TransitStopFacility.class);

		TransitStopFacility facility = this.schedule.getFacilities().get(stopId);

		if (facility == null){
			return;
		}

		if (change.newId != null && !change.newId.isBlank()) {
			this.schedule.removeStopFacility(facility);
			TransitStopFacility updated = this.schedule.getFactory().createTransitStopFacility(
				Id.create(change.newId, TransitStopFacility.class),
				facility.getCoord(), facility.getIsBlockingLane()
			);
			updated.setName(facility.getName());
			updated.setLinkId(facility.getLinkId());
			updated.setStopAreaId(facility.getStopAreaId());

			AttributesUtils.copyAttributesFromTo(facility, updated);

			facility = updated;
		}

		if (change.newLinkId != null && !change.newLinkId.isBlank()){
			facility.setLinkId(Id.create(change.newLinkId, Link.class));
		}

		if (change.newName != null && !change.newName.isBlank()){
			facility.setName(change.newName);
		}

		if(change.newCoord != null){
			facility.setCoord(new Coord(change.newCoord));
		}
	}

	private void applyChange(DeleteTransitStopPayload change) {
		Id<TransitStopFacility> stopId = Id.create(change.id, TransitStopFacility.class);
		this.schedule.removeStopFacility(this.schedule.getFacilities().get(stopId));

		List<TransitLine> linesToDelete = new ArrayList<>();
		for (TransitLine line : this.schedule.getTransitLines().values()) {
			List<TransitRoute> routesToUpdate = new ArrayList<>();
			List<TransitRoute> routesToDelete = new ArrayList<>();

			line.getRoutes().values().forEach(transitRoute -> {

				TransitRoute updatedRoute = null;
				if(transitRoute.getStops().stream().anyMatch(stop -> stop.getStopFacility().getId().equals(Id.create(change.id, TransitStopFacility.class)))){
          // make sure list is mutable
					List<TransitRouteStop> stops = new ArrayList<>(transitRoute.getStops());
					stops.removeIf(stop -> stop.getStopFacility().getId().equals(Id.create(change.id, TransitStopFacility.class)));
					updatedRoute = new TransitScheduleFactoryImpl().createTransitRoute(transitRoute.getId(), transitRoute.getRoute(), stops, transitRoute.getTransportMode());
				}

				if(updatedRoute != null){
					routesToUpdate.add(updatedRoute);
				}

				if (updatedRoute != null && updatedRoute.getStops().isEmpty()) {
					routesToDelete.add(transitRoute);
				}
			});

			routesToUpdate.forEach(route -> {
				line.removeRoute(route);
				line.addRoute(route);
			});
			routesToDelete.forEach(line::removeRoute);

			if (line.getRoutes().isEmpty()) {
				linesToDelete.add(line);
			}
		}
		linesToDelete.forEach(this.schedule::removeTransitLine);
	}

	private void applyChange(TransitStopGroupPayload change) {
		for (Change ch : change.modifications) {
			applyChange(ch);
		}
	}

	private void applyChange(AddTransitLinePayload change) {
		TransitLine line = this.schedule.getFactory().createTransitLine(Id.create(change.line.id, TransitLine.class));
		line.setName(change.line.name);

		for (PayloadTransitRoute route : change.line.transitRoutes.values()) {
			line.addRoute(toTransitRoute(this.schedule.getFactory(), route));
		}

		this.schedule.addTransitLine(line);
	}

	private void applyChange(UpdateTransitLinePayload change) {

		TransitLine line = this.schedule.getTransitLines().get(Id.create(change.lineId, TransitLine.class));

		this.schedule.removeTransitLine(line);

		TransitLine updatedLine = this.schedule.getFactory().createTransitLine(
			(change.newLineId == null || change.newLineId.isBlank()) ? line.getId() : Id.create(change.newLineId, TransitLine.class)
		);

		if (change.newName != null && !change.newName.isBlank())
			updatedLine.setName(change.newName);

		line.getRoutes().values().forEach(updatedLine::addRoute);

		AttributesUtils.copyAttributesFromTo(line, updatedLine);

		this.schedule.addTransitLine(updatedLine);
	}

	private void applyChange(DeleteTransitLinePayload change) {
		this.schedule.removeTransitLine(this.schedule.getTransitLines().get(Id.create(change.lineId, TransitLine.class)));
	}

	private void applyChange(TransitLineGroupPayload change) {
		for (Change ch : change.modifications) {
			applyChange(ch);
		}
	}

	private void applyChange(AddTransitRoutePayload change) {
		TransitLine line = this.schedule.getTransitLines().get(Id.create(change.lineId, TransitLine.class));
		line.addRoute(toTransitRoute(this.schedule.getFactory(), change.route));
	}

	private void applyChange(UpdateTransitRoutePayload change) {
		TransitLine line = this.schedule.getTransitLines().get(Id.create(change.lineId, TransitLine.class));

		TransitRoute deletedRoute = line.getRoutes().get(Id.create(change.routeId, TransitRoute.class));
		line.removeRoute(deletedRoute);

		TransitRoute updatedRoute = toTransitRoute(schedule.getFactory(), change.route, getNetworkRoute(change.route));
		line.addRoute(updatedRoute);
	}

	private void applyChange(DeleteTransitRoutePayload change) {
		TransitLine line = this.schedule.getTransitLines().get(Id.create(change.lineId, TransitLine.class));

		TransitRoute deletedRoute = line.getRoutes().get(Id.create(change.routeId, TransitRoute.class));
		line.removeRoute(deletedRoute);
	}

	private void applyChange(AddTransitRouteStopPayload change) {
		TransitLine line = this.schedule.getTransitLines().get(Id.create(change.lineId, TransitLine.class));
		TransitRoute route = line.getRoutes().get(Id.create(change.routeId, TransitRoute.class));

		TransitStopFacility stopFacility = this.schedule.getFacilities().get(Id.create(change.stopId, TransitStopFacility.class));

		int position = -1;
		Id<TransitStopFacility> afterStopId = Id.create(change.insertAfterStopId, TransitStopFacility.class);
		int afterOccurrence = change.insertAfterOccurrence == null ? 1 : change.insertAfterOccurrence;
		int occurrenceCounter = 0;

		List<TransitRouteStop> stops = new ArrayList<>(route.getStops());

		for (int i = 0, n = stops.size(); i < n; i++) {
			TransitRouteStop stop = stops.get(i);
			if (stop.getStopFacility().getId().equals(afterStopId)) {
				occurrenceCounter++;
				if (occurrenceCounter >= afterOccurrence) {
					position = i;
					break;
				}
			}
		}

		TransitRouteStop stopBefore = position >= 0 ? stops.get(position) : null;
		TransitRouteStop stopAfter = (position + 1) < stops.size() ? stops.get(position + 1) : null;

		double arrOffset = Double.NaN;
		if (stopBefore != null && stopAfter != null) {
			double depOffsetBefore = Double.isFinite(stopBefore.getDepartureOffset().orElse(Double.NaN)) ? stopBefore.getDepartureOffset().seconds() : stopBefore.getArrivalOffset().orElse(0);
			double arrOffsetAfter = Double.isFinite(stopAfter.getArrivalOffset().orElse(Double.NaN)) ? stopAfter.getArrivalOffset().seconds() : stopAfter.getDepartureOffset().orElse(0);
			arrOffset = depOffsetBefore + (arrOffsetAfter - depOffsetBefore) / 2.0;
		} else if (stopBefore != null) { // insert at end
			double depOffsetBefore = Double.isFinite(stopBefore.getDepartureOffset().orElse(Double.NaN)) ? stopBefore.getDepartureOffset().seconds() : stopBefore.getArrivalOffset().orElse(0);
			arrOffset = depOffsetBefore + 60;
//			if (!Double.isFinite(stopBefore.getDepartureOffset())) {
//				stopBefore.setDepartureOffset(stopBefore.getArrivalOffset());
//			}
		} else if (stopAfter != null) { // insert at start
			double arrOffsetAfter = Double.isFinite(stopAfter.getArrivalOffset().orElse(Double.NaN)) ? stopAfter.getArrivalOffset().seconds() : stopAfter.getDepartureOffset().orElse(0);
			arrOffset = arrOffsetAfter;
//			if (!Double.isFinite(stopAfter.getArrivalOffset())) {
//				stopAfter.setArrivalOffset(stopAfter.getDepartureOffset());
//			}
		}

		TransitRouteStop routeStop = this.schedule.getFactory().createTransitRouteStop(stopFacility, arrOffset, arrOffset);

		line.removeRoute(route);

		stops.add(position + 1, routeStop);

		// re-add with one more stop
		TransitRoute copy = this.schedule.getFactory().createTransitRoute(route.getId(), route.getRoute(), stops, route.getTransportMode());
		copy.setDescription(route.getDescription());
		route.getDepartures().values().forEach(copy::addDeparture);
		AttributesUtils.copyAttributesFromTo(route, copy);

		line.addRoute(copy);
	}

	private void applyChange(UpdateTransitRouteStopPayload change) {
		TransitLine line = this.schedule.getTransitLines().get(Id.create(change.lineId, TransitLine.class));
		TransitRoute route = line.getRoutes().get(Id.create(change.routeId, TransitRoute.class));

		Id<TransitStopFacility> stopId = Id.create(change.stop.id, TransitStopFacility.class);
		int occurrence = change.occurrence == null ? 1 : change.occurrence;
		int occurrenceCounter = 0;
		List<TransitRouteStop> stops = new ArrayList<>(route.getStops());
		for (int i = 0, n = stops.size(); i < n; i++) {
			TransitRouteStop stop = stops.get(i);
			if (stop.getStopFacility().getId().equals(stopId)) {
				occurrenceCounter++;
				if (occurrenceCounter == occurrence) {
					stops.set(i, toTransitRouteStop(schedule.getFactory(), change.stop, occurrence));
					break;
				}
			}
		}

		TransitRoute newRoute = this.schedule.getFactory().createTransitRoute(route.getId(), route.getRoute(), stops, route.getTransportMode());
		newRoute.setDescription(route.getDescription());
		route.getDepartures().values().forEach(newRoute::addDeparture);
		AttributesUtils.copyAttributesFromTo(route, newRoute);
		line.removeRoute(route);
		line.addRoute(newRoute);
	}

	private void applyChange(DeleteTransitRouteStopPayload change) {
		TransitLine line = this.schedule.getTransitLines().get(Id.create(change.lineId, TransitLine.class));
		TransitRoute route = line.getRoutes().get(Id.create(change.routeId, TransitRoute.class));

		Id<TransitStopFacility> stopId = Id.create(change.stopId, TransitStopFacility.class);

		int occurrence = change.occurrence == null ? 1 : change.occurrence;
		int occurrenceCounter = 0;
		List<TransitRouteStop> stops = route.getStops();
		List<TransitRouteStop> newStops = new ArrayList<>();
		for (TransitRouteStop stop : stops) {
			if (stop.getStopFacility().getId().equals(stopId)) {
				occurrenceCounter++;
				if (occurrenceCounter != occurrence) {
					newStops.add(stop);
				}
			} else {
				newStops.add(stop);
			}
		}

		TransitRoute newRoute = this.schedule.getFactory().createTransitRoute(route.getId(), route.getRoute(), newStops, route.getTransportMode());
		newRoute.setDescription(route.getDescription());
		route.getDepartures().values().forEach(newRoute::addDeparture);
		AttributesUtils.copyAttributesFromTo(route, newRoute);
		line.removeRoute(route);
		line.addRoute(newRoute);
	}

	private void applyChange(DeleteTransitNetworkRouteLinkPayload change) {
		TransitLine line = this.schedule.getTransitLines().get(Id.create(change.lineId, TransitLine.class));
		TransitRoute route = line.getRoutes().get(Id.create(change.routeId, TransitRoute.class));

		List<Id<Link>> linkIds = getAllLinksAsList(route);
		Id<Link> deleteLinkId = Id.create(change.linkId, Link.class);

		int occurrence = change.occurrence == null ? 1 : change.occurrence;
		int occurrenceCounter = 0;

		for (int i = 0, n = linkIds.size(); i < n; i++) {
			Id<Link> link = linkIds.get(i);
			if (link.equals(deleteLinkId)) {
				occurrenceCounter++;
				if (occurrenceCounter == occurrence) {
					linkIds.remove(i);
					break;
				}
			}
		}

		if(linkIds.isEmpty()){
			route.getRoute().setLinkIds(null,Collections.EMPTY_LIST,null);
		} else if(linkIds.size() < 3) {
			route.getRoute().setLinkIds(linkIds.get(0),Collections.EMPTY_LIST, linkIds.get(linkIds.size()-1));
		} else {
			route.getRoute().setLinkIds(linkIds.get(0), linkIds.subList(1, linkIds.size()-1), linkIds.get(linkIds.size()-1));
		}
	}

	private void applyChange(AddTransitNetworkRouteLinkPayload change) {

		TransitLine line = this.schedule.getTransitLines().get(Id.create(change.lineId, TransitLine.class));
		TransitRoute route = line.getRoutes().get(Id.create(change.routeId, TransitRoute.class));
		List<Id<Link>> linkIds = getAllLinksAsList(route);

		Id<Link> addToLinkId = Id.create(change.linkId, Link.class);

		int occurrence = change.occurrence == null ? 1 : change.occurrence;
		int occurrenceCounter = 0;

		for (int i = 0, n = linkIds.size(); i < n; i++) {
			Id<Link> link = linkIds.get(i);
			if (link.equals(addToLinkId)) {
				occurrenceCounter++;
				if (occurrenceCounter == occurrence) {
					linkIds.add(i + (change.before? 0 : 1), Id.create(change.newLinkId, Link.class));
					break;
				}
			}
		}

		if (linkIds.isEmpty()) {
			// if the initial route was completely empty, we won't add the link in the loop above, so we initialise the route to the single link here
			route.getRoute().setLinkIds(Id.create(change.newLinkId, Link.class), Collections.EMPTY_LIST, Id.create(change.newLinkId, Link.class));
		} else if (linkIds.size() < 3) {
			route.getRoute().setLinkIds(linkIds.get(0), Collections.EMPTY_LIST, linkIds.get(linkIds.size() - 1));
		} else {
			route.getRoute().setLinkIds(linkIds.get(0), linkIds.subList(1, linkIds.size() - 1), linkIds.get(linkIds.size() - 1));
		}

	}

	private List<Id<Link>> getAllLinksAsList(TransitRoute route) {
		List<Id<Link>> linkIds = new ArrayList<>();
		if (route.getRoute().getLinkIds() == null || route.getRoute().getLinkIds().isEmpty()) {
			// handle edge case of empty network route (can happen during edit)
			if (Objects.equals(route.getRoute().getStartLinkId(), null)) {
				return linkIds;
			}

			// handle edge case where network route is only one link "l42", prevent returning ["l42", "l42"]
			if (Objects.equals(route.getRoute().getStartLinkId(), route.getRoute().getEndLinkId())) {
				linkIds.add(route.getRoute().getStartLinkId());
				return linkIds;
			}
		}

		linkIds.add(route.getRoute().getStartLinkId());
		linkIds.addAll(route.getRoute().getLinkIds());
		linkIds.add(route.getRoute().getEndLinkId());
		return linkIds;
	}

	private void applyChange(UpdateTransitRouteDeparturesPayload change) {
		TransitLine line = this.schedule.getTransitLines().get(Id.create(change.lineId, TransitLine.class));
		TransitRoute route = line.getRoutes().get(Id.create(change.routeId, TransitRoute.class));

		new ArrayList<>(route.getDepartures().values()).forEach(route::removeDeparture);

		for (PayloadTransitDeparture departure : change.departures) {
			Departure d = this.schedule.getFactory().createDeparture(Id.create(departure.id, Departure.class), departure.departureTime);
			d.setVehicleId(determineAppropriateVehicle(departure, route, line));
			route.addDeparture(d);
		}
	}

	private Id<Vehicle> createVehicleId(String vehicleId) {
		if (vehicleId == null || vehicleId.isBlank()) {
			return Id.createVehicleId(UUID.randomUUID().toString());
		}
		return Id.createVehicleId(vehicleId);
	}

	private Id<Vehicle> determineAppropriateVehicle(PayloadTransitDeparture departure, TransitRoute route, TransitLine line) {
		Map<Id<Vehicle>, Vehicle> vehicles = this.scenario.getTransitVehicles().getVehicles();
		Map<Id<VehicleType>, VehicleType> vehicleTypes = this.scenario.getTransitVehicles().getVehicleTypes();

		boolean vehicleIdProvided = false;
		Vehicle existingVehicle = null;
		// the user provided a vehicle id
		if (departure.vehicleRefId != null && !departure.vehicleRefId.isBlank()) {
			vehicleIdProvided = true;
			existingVehicle = vehicles.get(Id.create(departure.vehicleRefId, Vehicle.class));
		}

		boolean vehicleTypeProvided = false;
		VehicleType existingType = null;
		// the user provided a type
		if (departure.vehicleType != null && !departure.vehicleType.isBlank()) {
			vehicleTypeProvided = true;
			existingType =  vehicleTypes.get(Id.create(departure.vehicleType, VehicleType.class));
		}

		if(existingVehicle!=null){
			// vehicle with given id exists, so we'll use it

			// in case a type was provided as well, but it disagrees with the existing vehicle type
			if (existingType != null && !existingVehicle.getType().getId().equals(existingType.getId())) {
				log.warn("Changeset contains modification with updated departure {} in transit line: {} and route: {} with a transit vehicle with id {} and vehicle type {}, but existing vehicle in file {} has type {}",
					departure.id,
					line.getId(),
					route.getId(),
					departure.vehicleRefId,
					departure.vehicleType,
					this.scenario.getConfig().transit().getVehiclesFile(),
					vehicles.get(Id.create(departure.vehicleRefId, Vehicle.class)).getType().getId()
				);
			}

			return Id.createVehicleId(departure.vehicleRefId);

		} else if(existingType!=null) {
			// the type exists, but the id does not exist yet, so we'll create a vehicle with the new id
			Id<Vehicle> vehicleId = vehicleIdProvided
				? Id.createVehicleId(departure.vehicleRefId)
				: Id.createVehicleId(UUID.randomUUID().toString());

			Vehicle newVehicle = scenario.getTransitVehicles().getFactory().createVehicle(vehicleId, existingType);
			scenario.getTransitVehicles().addVehicle(newVehicle);

			return vehicleId;

		} else {
			// neither type nor id exists

			VehicleType type = vehicleTypeProvided
				? createVehicleType(Id.create(departure.vehicleType, VehicleType.class))
				: getFallbackOrCreateVehicleType(line);
			Id<Vehicle> vehicleId = vehicleIdProvided
				? Id.createVehicleId(departure.vehicleRefId)
				: Id.createVehicleId(UUID.randomUUID().toString());

			Vehicle newVehicle = scenario.getTransitVehicles().getFactory().createVehicle(vehicleId, type);
			scenario.getTransitVehicles().addVehicle(newVehicle);

			return vehicleId;
		}

	}

	private VehicleType getFallbackOrCreateVehicleType(TransitLine line) {

		Map<Id<Vehicle>, Vehicle> vehicles = this.scenario.getTransitVehicles().getVehicles();
		Map<Id<VehicleType>, VehicleType> vehicleTypes = this.scenario.getTransitVehicles().getVehicleTypes();

		for (TransitRoute routeInLine : line.getRoutes().values()) {
			for (Departure dep : routeInLine.getDepartures().values()) {
				if (dep.getVehicleId() != null) {
					Vehicle vehicle = vehicles.get(dep.getVehicleId());
					VehicleType existingVehType = vehicle == null ? null : vehicle.getType();
					if (existingVehType != null) {
						return existingVehType;
					}
				}
			}
		}

		Id<VehicleType> fallbackId = Id.create("tramolaPT", VehicleType.class);
		VehicleType existingVehType = vehicleTypes.get(fallbackId);
		if (existingVehType != null) {
			return existingVehType;
		}

		return createVehicleType(fallbackId);
	}

	private VehicleType createVehicleType(Id<VehicleType> id) {
		VehicleType vehType = VehicleUtils.createVehicleType(id);
		vehType.setDescription("Default transit vehicle type generated by Tramola");
		vehType.getCapacity().setSeats(100);
		vehType.getCapacity().setStandingRoom(30);
		vehType.setPcuEquivalents(5);

		scenario.getTransitVehicles().addVehicleType(vehType);

		return vehType;
	}


	private TransitRoute toTransitRoute(TransitScheduleFactory factory, PayloadTransitRoute route) {
        return toTransitRoute(factory, route, getNetworkRoute(route));
	}

	private static NetworkRoute getNetworkRoute(PayloadTransitRoute route) {
		if (route.linkIds == null) {
			return null;
		}
		Id<Link> start = null;
		Id<Link> end = null;
		List<Id<Link>> middle = new ArrayList<>();
		for (int i = 0; i < route.linkIds.length; i++) {
			if (i == 0) {
				start = Id.createLinkId(route.linkIds[i]);
			}
			if (i == route.linkIds.length - 1) {
				end = Id.createLinkId(route.linkIds[i]);
			}
			if (i != 0 && i != route.linkIds.length - 1) {
				middle.add(Id.createLinkId(route.linkIds[i]));
			}
		}
		NetworkRoute networkRoute = RouteUtils.createLinkNetworkRouteImpl(start, middle, end);
		return networkRoute;
	}

	private TransitRoute toTransitRoute(TransitScheduleFactory factory, PayloadTransitRoute route, NetworkRoute networkRoute) {
		List<TransitRouteStop> routeStops = new ArrayList<>();
		Map<String, Integer> occurrencies = new HashMap<>();
		for (PayloadTransitRouteStop stop : route.stops) {
			routeStops.add(
				toTransitRouteStop(
					factory, stop, occurrencies.compute(stop.id, (id, existing) -> existing == null ? 1 : (1 + existing))
				)
			);
		}

		TransitRoute transitRoute = factory.createTransitRoute(Id.create(route.id, TransitRoute.class), networkRoute, routeStops, route.mode);
		transitRoute.setDescription(route.name);

		for (PayloadTransitDeparture departure : route.departures) {
			Departure d = factory.createDeparture(Id.create(departure.id, Departure.class), departure.departureTime);
			d.setVehicleId(createVehicleId(departure.vehicleRefId));
			transitRoute.addDeparture(d);
		}
		return transitRoute;
	}

	private TransitRouteStop toTransitRouteStop(TransitScheduleFactory factory, PayloadTransitRouteStop stop, int occurrence) {
		// TODO: occurrence is not used
		TransitRouteStop transitRouteStop = factory.createTransitRouteStop(
			this.schedule.getFacilities().get(Id.create(stop.id, TransitStopFacility.class)),
			Double.isFinite(stop.arrivalOffset) ? OptionalTime.defined(stop.arrivalOffset) : OptionalTime.undefined(),
			Double.isFinite(stop.departureOffset) ? OptionalTime.defined(stop.departureOffset) : OptionalTime.undefined()
		);
		transitRouteStop.setAwaitDepartureTime(stop.awaitDeparture);
		return transitRouteStop;
	}


	private record RemovedLink(Id<Link> id, Id<Node> fromNode, Id<Node> toNode) {
	}
}
