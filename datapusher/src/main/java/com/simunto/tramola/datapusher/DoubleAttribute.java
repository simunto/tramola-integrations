package com.simunto.tramola.datapusher;

/**
 * @author mrieser
 */
class DoubleAttribute extends Attribute {

	final double value;

	DoubleAttribute(String name, double value) {
		super(name);
		this.value = value;
	}
}
