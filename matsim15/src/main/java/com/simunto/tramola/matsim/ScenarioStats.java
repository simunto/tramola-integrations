package com.simunto.tramola.matsim;

import com.simunto.tramola.datapusher.Reading;
import com.simunto.tramola.datapusher.Run;
import com.simunto.tramola.datapusher.Serie;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Population;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.pt.transitSchedule.api.TransitSchedule;
import org.matsim.vehicles.Vehicles;

/**
 * @author mrieser / Simunto GmbH
 */
public class ScenarioStats {

	private ScenarioStats() {
	}

	public static void submit(Scenario scenario, Run run) {
		Serie serie = run.addSerie("scenarioStats", Serie.SerieType.LONG);

		Reading.ReadingBuilder reading = Reading.build();

		Network network = scenario.getNetwork();
		reading.value("linkCount", network.getLinks().size());
		reading.value("nodeCount", network.getNodes().size());

		Population population = scenario.getPopulation();
		reading.value("populationSize", population.getPersons().size());

		ActivityFacilities facilities = scenario.getActivityFacilities();
		reading.value("facilityCount", facilities.getFacilities().size());

		TransitSchedule schedule = scenario.getTransitSchedule();
		reading.value("transitStopCount", schedule.getFacilities().size());
		reading.value("transitLineCount", schedule.getTransitLines().size());

		Vehicles transitVehicles = scenario.getTransitVehicles();
		reading.value("transitVehicleCount", transitVehicles.getVehicles().size());

		serie.addReading(reading.get());
	}

}
