package com.simunto.tramola.datapusher;

// based on code from http://stackoverflow.com/questions/1835430/byte-order-mark-screws-up-file-reading-in-java

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

/**
 * The <code>UnicodeBOMInputStream</code> class wraps any
 * <code>InputStream</code> and detects the presence of any Unicode BOM (Byte
 * Order Mark) at its beginning, as defined by <a
 * href="http://www.faqs.org/rfcs/rfc3629.html">RFC 3629 - UTF-8, a
 * transformation format of ISO 10646</a>
 *
 * <p>
 * The <a href="http://www.unicode.org/unicode/faq/utf_bom.html">Unicode FAQ</a>
 * defines 5 types of BOMs:
 * <ul>
 * <li>
 *
 * <pre>
 * 00 00 FE FF  = UTF-32, big-endian
 * </pre>
 *
 * </li>
 * <li>
 *
 * <pre>
 * FF FE 00 00  = UTF-32, little-endian
 * </pre>
 *
 * </li>
 * <li>
 *
 * <pre>
 * FE FF        = UTF-16, big-endian
 * </pre>
 *
 * </li>
 * <li>
 *
 * <pre>
 * FF FE        = UTF-16, little-endian
 * </pre>
 *
 * </li>
 * <li>
 *
 * <pre>
 * EF BB BF     = UTF-8
 * </pre>
 *
 * </li>
 * </ul>
 * </p>
 *
 * <p>
 * Use the {@link #getBOM()} method to know whether a BOM has been detected or
 * not.
 * </p>
 */
class UnicodeInputStream extends InputStream {

	private final PushbackInputStream in;
	private final BOM bom;

	/**
	 * Wraps the existing inputStream and tries to detect if it contains a Byte Order Mark (BOM).
	 * If it does, the BOM is skipped by default.
	 *
	 * @param inputStream
	 * @throws NullPointerException
	 * @throws IOException
	 */
	UnicodeInputStream(final InputStream inputStream) throws NullPointerException, IOException {
		this(inputStream, true);
	}

	private UnicodeInputStream(final InputStream inputStream, final boolean skipBom) throws NullPointerException, IOException {
		if (inputStream == null) {
			throw new NullPointerException("invalid input stream: null is not allowed");
		}

		this.in = new PushbackInputStream(inputStream, 4);

		final byte[] bytes = new byte[4];
		final int read = this.in.read(bytes);

		switch (read) {
			case 4:
				if ((bytes[0] == (byte) 0xFF) && (bytes[1] == (byte) 0xFE)
						&& (bytes[2] == (byte) 0x00) && (bytes[3] == (byte) 0x00)) {
					this.bom = BOM.UTF_32_LE;
					break;
				} else if ((bytes[0] == (byte) 0x00) && (bytes[1] == (byte) 0x00)
						&& (bytes[2] == (byte) 0xFE) && (bytes[3] == (byte) 0xFF)) {
					this.bom = BOM.UTF_32_BE;
					break;
				}

			case 3:
				if ((bytes[0] == (byte) 0xEF) && (bytes[1] == (byte) 0xBB)
						&& (bytes[2] == (byte) 0xBF)) {
					this.bom = BOM.UTF_8;
					break;
				}

			case 2:
				if ((bytes[0] == (byte) 0xFF) && (bytes[1] == (byte) 0xFE)) {
					this.bom = BOM.UTF_16_LE;
					break;
				} else if ((bytes[0] == (byte) 0xFE) && (bytes[1] == (byte) 0xFF)) {
					this.bom = BOM.UTF_16_BE;
					break;
				}

			default:
				this.bom = BOM.NONE;
				break;
		}

		if (read > 0) {
			this.in.unread(bytes, 0, read);
		}
		if (skipBom) {
			this.in.skip(this.bom.bytes.length);
		}
	}

	private final BOM getBOM() {
		return this.bom;
	}

	@Override
	public int read() throws IOException {
		return this.in.read();
	}

	@Override
	public int read(final byte[] b) throws IOException, NullPointerException {
		return this.in.read(b, 0, b.length);
	}

	@Override
	public int read(final byte[] b, final int off, final int len)
			throws IOException, NullPointerException {
		return this.in.read(b, off, len);
	}

	@Override
	public long skip(final long n) throws IOException {
		return this.in.skip(n);
	}

	@Override
	public int available() throws IOException {
		return this.in.available();
	}

	@Override
	public void close() throws IOException {
		this.in.close();
	}

	@Override
	public synchronized void mark(final int readlimit) {
		this.in.mark(readlimit);
	}

	@Override
	public synchronized void reset() throws IOException {
		this.in.reset();
	}

	@Override
	public boolean markSupported() {
		return this.in.markSupported();
	}

	public static final class BOM {
		static final BOM NONE = new BOM(new byte[] {}, "NONE");
		static final BOM UTF_8 = new BOM(new byte[] { (byte) 0xEF, (byte) 0xBB, (byte) 0xBF }, "UTF-8");
		static final BOM UTF_16_LE = new BOM(new byte[] { (byte) 0xFF, (byte) 0xFE }, "UTF-16 little-endian");
		static final BOM UTF_16_BE = new BOM(new byte[] { (byte) 0xFE, (byte) 0xFF }, "UTF-16 big-endian");
		static final BOM UTF_32_LE = new BOM(new byte[] { (byte) 0xFF, (byte) 0xFE, (byte) 0x00, (byte) 0x00 }, "UTF-32 little-endian");
		static final BOM UTF_32_BE = new BOM(new byte[] { (byte) 0x00, (byte) 0x00, (byte) 0xFE, (byte) 0xFF }, "UTF-32 big-endian");

		final byte[] bytes;
		private final String description;

		@Override
		public final String toString() {
			return this.description;
		}

		public final byte[] getBytes() {
			final int length = this.bytes.length;
			final byte[] result = new byte[length];

			// Make a defensive copy
			System.arraycopy(this.bytes, 0, result, 0, length);

			return result;
		}

		private BOM(final byte[] bom, final String description) {
			this.bytes = bom;
			this.description = description;
		}
	}

}