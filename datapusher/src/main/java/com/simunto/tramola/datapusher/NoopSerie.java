package com.simunto.tramola.datapusher;

/**
 * @author mrieser / Simunto GmbH
 */
class NoopSerie extends Serie {

	NoopSerie(Run run, String seriesName) {
		super(run, seriesName, SerieType.STRING);
	}

	@Override
	public void addReading(Reading reading) {
	}
}
