package com.simunto.tramola.changeset;

import com.google.common.io.Resources;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.net.URL;
import java.util.Map;

public class ChangesetIOTest {


    @Test
    public void io() throws IOException {

        Changeset cs = new Changeset("somename",
                new Change("ds", new Change.NetworkGroupPayload("something", new Change[0])),
                new Change("ds", new Change.AddLinkPayload("id", "from", "to", Map.of())),
                new Change("ds", new Change.DeleteNodePayload("d")),
                new Change("ds", new Change.DeleteLinkPayload("d"))
        );

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(out);

        Changeset.write(writer, cs);

        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
        InputStreamReader reader = new InputStreamReader(in);

        Changeset copy = Changeset.read(reader);

        Assert.assertEquals(cs, copy);
    }

    @Test
    public void readFromTramola() throws IOException {

        URL url = Resources.getResource("com/simunto/tramola/changeset/Test.json");

        try (InputStreamReader reader = new InputStreamReader(url.openStream())) {

            Changeset cs = Changeset.read(reader);

            System.out.println(cs);

            Assert.assertEquals(24, cs.modifications.length);

        }

        URL url2 = Resources.getResource("com/simunto/tramola/changeset/Test2.json");

        try (InputStreamReader reader = new InputStreamReader(url2.openStream())) {
            Changeset cs = Changeset.read(reader);
            Assert.assertEquals(67, cs.modifications.length);
        }
    }

    @Test
    public void readFromTramola23_2_0() throws IOException {
        URL url = Resources.getResource("com/simunto/tramola/changeset/tramola_23_2_0_changeset.json");
        try (InputStreamReader reader = new InputStreamReader(url.openStream())) {
            Changeset cs = Changeset.read(reader);
            System.out.println(cs);
            Assert.assertEquals(11, cs.modifications.length);
        }
    }
}
