package com.simunto.tramola.datapusher;

/**
 * @author mrieser
 */
class Attribute {

	final String name;

	Attribute(String name) {
		this.name = name;
	}
}
