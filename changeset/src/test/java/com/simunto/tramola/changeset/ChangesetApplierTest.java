package com.simunto.tramola.changeset;


import org.junit.Assert;
import org.junit.Test;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.network.NetworkFactory;
import org.matsim.api.core.v01.network.Node;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.population.routes.NetworkRoute;
import org.matsim.core.population.routes.RouteUtils;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.utils.geometry.transformations.IdentityTransformation;
import org.matsim.core.utils.misc.OptionalTime;
import org.matsim.pt.transitSchedule.api.*;
import org.matsim.vehicles.Vehicle;
import org.matsim.vehicles.VehicleType;
import org.matsim.vehicles.VehicleUtils;
import org.matsim.vehicles.Vehicles;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.simunto.tramola.changeset.Change.*;


public class ChangesetApplierTest {

	@Test
	public void testApplyChanges_emptyChanges() {
		Scenario scenario = createScenario();

		TransitSchedule schedule = scenario.getTransitSchedule();
		Vehicles transitVehicles = null;

		List<Changeset> changesets = new ArrayList<>();

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(1, schedule.getTransitLines().size());
		Assert.assertEquals(3, schedule.getFacilities().size());
	}

	@Test
	public void testApplyChanges_updateTransitRouteDepartures() {
		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1to3").getDepartures().size());

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test", new Change("transitSchedule", new UpdateTransitRouteDeparturesPayload("blue", "b1to3", new PayloadTransitDeparture[]{
				new PayloadTransitDeparture("d1", 6 * 3600 + 0 * 1800 + 60, null, null), // should create default vehicle
				new PayloadTransitDeparture("d2", 6 * 3600 + 0 * 1800 + 60, null, "handcar"), // should create vehicle type
				new PayloadTransitDeparture("d3", 6 * 3600 + 0 * 1800 + 60, "h1", "handcar"), // should create vehicle type
			})))
		);

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(3, getRoute(schedule, "blue", "b1to3").getDepartures().size());
		Assert.assertEquals(Id.create("tramolaPT", VehicleType.class), scenario.getTransitVehicles().getVehicles().get(getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d1", Departure.class)).getVehicleId()).getType().getId());
		Assert.assertEquals(Id.create("handcar", VehicleType.class), scenario.getTransitVehicles().getVehicles().get(getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d2", Departure.class)).getVehicleId()).getType().getId());
		Assert.assertEquals(Id.createVehicleId("h1"), getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d3", Departure.class)).getVehicleId());
		Assert.assertEquals(Id.create("handcar", VehicleType.class), scenario.getTransitVehicles().getVehicles().get(getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d3", Departure.class)).getVehicleId()).getType().getId());

		// Add some existing vehTypes and cover other cases

		VehicleType rocketVehType = VehicleUtils.createVehicleType(Id.create("rocket", VehicleType.class));
		rocketVehType.setDescription("Space Shuttle");
		rocketVehType.getCapacity().setSeats(1);
		rocketVehType.getCapacity().setStandingRoom(0);
		rocketVehType.setPcuEquivalents(5);
		scenario.getTransitVehicles().addVehicleType(rocketVehType);

		VehicleType balloonVehType = VehicleUtils.createVehicleType(Id.create("balloon", VehicleType.class));
		balloonVehType.setDescription("Space Shuttle");
		balloonVehType.getCapacity().setSeats(1);
		balloonVehType.getCapacity().setStandingRoom(0);
		balloonVehType.setPcuEquivalents(5);
		scenario.getTransitVehicles().addVehicleType(balloonVehType);

		Vehicle rocket1 = scenario.getTransitVehicles().getFactory().createVehicle(Id.createVehicleId("rocket1"), rocketVehType);
		Vehicle rocket2 = scenario.getTransitVehicles().getFactory().createVehicle(Id.createVehicleId("rocket2"), rocketVehType);
		scenario.getTransitVehicles().addVehicle(rocket1);
		scenario.getTransitVehicles().addVehicle(rocket2);


		changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test", new Change("transitSchedule", new UpdateTransitRouteDeparturesPayload("blue", "b1to3", new PayloadTransitDeparture[]{
				new PayloadTransitDeparture("d1", 6 * 3600 + 0 * 1800 + 60, "rocket1", null), // should use that vehicle
				new PayloadTransitDeparture("d2", 6 * 3600 + 1 * 1800 + 60, "rocket1", "rocket"), // should also be fine
				new PayloadTransitDeparture("d3", 6 * 3600 + 2 * 1800 + 60, "rocket1", "balloon"), // should print warning
				new PayloadTransitDeparture("d4", 6 * 3600 + 3 * 1800 + 60, "rocket3", "rocket"), // should create new rocket
				new PayloadTransitDeparture("d5", 6 * 3600 + 4 * 1800 + 60, "balloon", "balloon"),
				new PayloadTransitDeparture("d6", 6 * 3600 + 5 * 1800 + 60, null, null),
			})))
		);

		applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(1, schedule.getTransitLines().size());
		Assert.assertEquals(3, schedule.getFacilities().size());
		Assert.assertEquals(6, getRoute(schedule, "blue", "b1to3").getDepartures().size());

		Assert.assertEquals(Id.createVehicleId("rocket1"), getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d1", Departure.class)).getVehicleId());
		Assert.assertEquals(Id.create("rocket", VehicleType.class), scenario.getTransitVehicles().getVehicles().get(getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d1", Departure.class)).getVehicleId()).getType().getId());

		Assert.assertEquals(Id.createVehicleId("rocket1"), getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d2", Departure.class)).getVehicleId());
		Assert.assertEquals(Id.create("rocket", VehicleType.class), scenario.getTransitVehicles().getVehicles().get(getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d2", Departure.class)).getVehicleId()).getType().getId());

		Assert.assertEquals(Id.createVehicleId("rocket1"), getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d3", Departure.class)).getVehicleId());
		Assert.assertEquals(Id.create("rocket", VehicleType.class), scenario.getTransitVehicles().getVehicles().get(getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d3", Departure.class)).getVehicleId()).getType().getId());

		Assert.assertEquals(Id.createVehicleId("rocket3"), getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d4", Departure.class)).getVehicleId());
		Assert.assertEquals(Id.create("rocket", VehicleType.class), scenario.getTransitVehicles().getVehicles().get(getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d4", Departure.class)).getVehicleId()).getType().getId());

		Assert.assertEquals(Id.createVehicleId("balloon"), getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d5", Departure.class)).getVehicleId());
		Assert.assertEquals(Id.create("balloon", VehicleType.class), scenario.getTransitVehicles().getVehicles().get(getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d5", Departure.class)).getVehicleId()).getType().getId());

		Assert.assertEquals(36, getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d6", Departure.class)).getVehicleId().toString().length());
		Assert.assertNotEquals(Id.create("tramolaPT", VehicleType.class), scenario.getTransitVehicles().getVehicles().get(getRoute(schedule, "blue", "b1to3").getDepartures().get(Id.create("d6", Departure.class)).getVehicleId()).getType().getId());
	}

	@Test
	public void testApplyChanges_updateTransitRouteStop() {
		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		TransitRoute route = getRoute(schedule, "blue", "b1to3");
		{
			TransitRouteStop stop2 = route.getStops().get(1);
			Assert.assertEquals(Id.create("pt2", TransitStopFacility.class), stop2.getStopFacility().getId());
			Assert.assertTrue(stop2.getArrivalOffset().isUndefined());
			Assert.assertEquals(150, stop2.getDepartureOffset().seconds(), 1e-7);
		}

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test",
				new Change("transitSchedule",
					new UpdateTransitRouteStopPayload(
						"blue",
						"b1to3",
						new PayloadTransitRouteStop("pt2", 120, 140, false),
						1
					)
				)
			)
		);

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		TransitRoute modifiedRoute = getRoute(schedule, "blue", "b1to3");
		TransitRouteStop modifiedStop2 = modifiedRoute.getStops().get(1);
		Assert.assertEquals(Id.create("pt2", TransitStopFacility.class), modifiedStop2.getStopFacility().getId());
		Assert.assertEquals(120, modifiedStop2.getArrivalOffset().seconds(), 1e-7);
		Assert.assertEquals(140, modifiedStop2.getDepartureOffset().seconds(), 1e-7);
	}

	@Test
	public void testApplyChanges_addMultipleRouteStops() {
		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1to3").getDepartures().size());

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test",
				new Change("transitSchedule",
					new AddTransitStopPayload("pt2.3", new PayloadTransitStop(new double[]{1000, 250}, false, null, "Detown-South", null))
				),
				new Change("transitSchedule",
					new AddTransitStopPayload("pt2.5", new PayloadTransitStop(new double[]{1000, 500}, false, null, "Detown", null))
				),
				new Change("transitSchedule",
					new AddTransitRouteStopPayload("blue", "b1to3", "pt2.5", "pt2", 1)
				),
				new Change("transitSchedule",
					new AddTransitRouteStopPayload("blue", "b1to3", "pt2.3", "pt2", 1)
				))
		);

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(1, schedule.getTransitLines().size());
		Assert.assertEquals(5, schedule.getFacilities().size());
		TransitRoute b1to3 = getRoute(schedule, "blue", "b1to3");

		Assert.assertEquals(5, b1to3.getStops().size());
		Assert.assertEquals("pt1", b1to3.getStops().get(0).getStopFacility().getId().toString());
		Assert.assertEquals("pt2", b1to3.getStops().get(1).getStopFacility().getId().toString());
		Assert.assertEquals("pt2.3", b1to3.getStops().get(2).getStopFacility().getId().toString());
		Assert.assertEquals("pt2.5", b1to3.getStops().get(3).getStopFacility().getId().toString());
		Assert.assertEquals("pt3", b1to3.getStops().get(4).getStopFacility().getId().toString());
	}

	@Test
	public void testApplyChanges_addRouteStop() {

		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1to3").getDepartures().size());

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test", new Change("transitSchedule",
				new AddTransitStopPayload("pt1.5", new PayloadTransitStop(new double[]{500, 100}, false, null, "Detown", null))
			),
				new Change("transitSchedule",
					new AddTransitRouteStopPayload("blue", "b1to3", "pt1.5", "pt1", 1)
				))
		);


		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(1, schedule.getTransitLines().size());
		Assert.assertEquals(4, schedule.getFacilities().size());
		TransitRoute b1to3 = getRoute(schedule, "blue", "b1to3");
		Assert.assertEquals(4, b1to3.getStops().size());
		Assert.assertEquals("pt1", b1to3.getStops().get(0).getStopFacility().getId().toString());
		Assert.assertEquals("pt1.5", b1to3.getStops().get(1).getStopFacility().getId().toString());
		Assert.assertEquals("pt2", b1to3.getStops().get(2).getStopFacility().getId().toString());
		Assert.assertEquals("pt3", b1to3.getStops().get(3).getStopFacility().getId().toString());
	}

	@Test
	public void testApplyChanges_moveStop() {

		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		TransitRoute route = getRoute(schedule, "blue", "b1to3");
		{
			TransitRouteStop stop2 = route.getStops().get(1);
			Assert.assertEquals(Id.create("pt2", TransitStopFacility.class), stop2.getStopFacility().getId());
			Assert.assertEquals(1000, stop2.getStopFacility().getCoord().getX(), 0.1);
			Assert.assertEquals(0, stop2.getStopFacility().getCoord().getY(), 0.1);
		}

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test",
				new Change("transitSchedule", new TransitStopGroupPayload(
					new Change("", new UpdateTransitStopPayload("pt2", null, null, null, new double[]{1234, 3456}))
				))
			));

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		TransitStopFacility s  = schedule.getFacilities().get(Id.create("pt2", TransitStopFacility.class));
				Assert.assertEquals(1234, s.getCoord().getX(), 0.1);
				Assert.assertEquals(3456, s.getCoord().getY(), 0.1);

				TransitRoute b1to3 = getRoute(schedule, "blue", "b1to3");
				TransitRouteStop stop2 = b1to3.getStops().get(1);
				Assert.assertEquals(1234, stop2.getStopFacility().getCoord().getX(), 0.1);
				Assert.assertEquals(3456, stop2.getStopFacility().getCoord().getY(), 0.1);

	}

	@Test
	public void testApplyChanges_deleteStop() {

		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		TransitRoute route = getRoute(schedule, "blue", "b1to3");
		{
			TransitRouteStop stop2 = route.getStops().get(1);
			Assert.assertEquals(Id.create("pt2", TransitStopFacility.class), stop2.getStopFacility().getId());
			Assert.assertEquals(1000, stop2.getStopFacility().getCoord().getX(), 0.1);
			Assert.assertEquals(0, stop2.getStopFacility().getCoord().getY(), 0.1);
		}

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test",
				new Change("transitSchedule", new TransitStopGroupPayload(
					new Change("", new DeleteTransitStopPayload("pt2"))
				))
			));

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		TransitStopFacility s  = schedule.getFacilities().get(Id.create("pt2", TransitStopFacility.class));
		Assert.assertNull(s);

		TransitRoute b1to3 = getRoute(schedule, "blue", "b1to3");
		Assert.assertEquals(2, b1to3.getStops().size());

		changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test",
				new Change("transitSchedule", new TransitStopGroupPayload(
					new Change("", new DeleteTransitStopPayload("pt1")),
					new Change("", new DeleteTransitStopPayload("pt3"))
				))
			));

		applier.applyChanges(changesets);

		Assert.assertEquals(0, scenario.getTransitSchedule().getTransitLines().size());
	}


	@Test
	public void testApplyChanges_updateRouteStopLink() {

		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		TransitRoute route = getRoute(schedule, "blue", "b1to3");
		{
			TransitRouteStop stop2 = route.getStops().get(1);
			Assert.assertEquals(Id.create("pt2", TransitStopFacility.class), stop2.getStopFacility().getId());
			Assert.assertEquals(1000, stop2.getStopFacility().getCoord().getX(), 0.1);
			Assert.assertEquals(0, stop2.getStopFacility().getCoord().getY(), 0.1);
		}

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test",
				new Change("transitSchedule", new TransitStopGroupPayload(
					new Change("", new UpdateTransitStopPayload("pt2", null, "34", null, null))
				))
			));

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		TransitRoute b1to3 = getRoute(schedule, "blue", "b1to3");
		TransitRouteStop stop2 = b1to3.getStops().get(1);
		Assert.assertEquals(Id.create("34", Link.class), stop2.getStopFacility().getLinkId());
	}

	@Test
	public void testApplyChanges_addRouteStopAtStart() {

		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1to3").getDepartures().size());

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test", new Change("transitSchedule",
				new AddTransitStopPayload("pt0.5", new PayloadTransitStop(new double[]{0, 500}, false, null, "Detown", null))
			),
				new Change("transitSchedule",
					new AddTransitRouteStopPayload("blue", "b1to3", "pt0.5", "insert-first", 1)
				))
		);

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(1, schedule.getTransitLines().size());
		Assert.assertEquals(4, schedule.getFacilities().size());
		TransitRoute b1to3 = getRoute(schedule, "blue", "b1to3");
		Assert.assertEquals(4, b1to3.getStops().size());
		Assert.assertEquals("pt0.5", b1to3.getStops().get(0).getStopFacility().getId().toString());
		Assert.assertEquals("pt1", b1to3.getStops().get(1).getStopFacility().getId().toString());
		Assert.assertEquals("pt2", b1to3.getStops().get(2).getStopFacility().getId().toString());
		Assert.assertEquals("pt3", b1to3.getStops().get(3).getStopFacility().getId().toString());
	}

	@Test
	public void testApplyChanges_addTransitRoute() {
		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1to3").getDepartures().size());

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test", new Change("transitSchedule", new AddTransitRoutePayload("blue", "b1direct3",
				new PayloadTransitRoute("blue", "b1direct3", "bus", null,
					new PayloadTransitRouteStop[]{
						new PayloadTransitRouteStop("pt1", Double.NaN, 0, false),
						new PayloadTransitRouteStop("pt3", 240, Double.NaN, false)
					},
					new String[]{"pt11", "12", "23", "34", "pt33"},
					new PayloadTransitDeparture[]{
						new PayloadTransitDeparture("d99", 7 * 3600, null, null),
						new PayloadTransitDeparture("d98", 8 * 3600, null, null),
						new PayloadTransitDeparture("d97", 9 * 3600, null, null)
					}
				)
			)))
		);

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(1, schedule.getTransitLines().size());
		Assert.assertEquals(3, schedule.getFacilities().size());
		Assert.assertEquals(2, schedule.getTransitLines().get(Id.create("blue", TransitLine.class)).getRoutes().size());

		Assert.assertNotNull(getRoute(schedule, "blue", "b1to3"));
		Assert.assertNotNull(getRoute(schedule, "blue", "b1direct3"));

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1to3").getDepartures().size());
		Assert.assertEquals(3, getRoute(schedule, "blue", "b1direct3").getDepartures().size());

		Assert.assertEquals("pt11", getRoute(schedule, "blue", "b1to3").getRoute().getStartLinkId().toString());
		Assert.assertEquals(3, getRoute(schedule, "blue", "b1to3").getRoute().getLinkIds().size());
		Assert.assertEquals("pt33", getRoute(schedule, "blue", "b1to3").getRoute().getEndLinkId().toString());
	}

	@Test
	public void testApplyChanges_addTransitRoute__backward_compatibility_with_23_2_0() {
		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1to3").getDepartures().size());

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test", new Change("transitSchedule", new AddTransitRoutePayload("blue", "b1direct3",
				new PayloadTransitRoute("blue", "b1direct3", "bus", null,
					new PayloadTransitRouteStop[]{
						new PayloadTransitRouteStop("pt1", Double.NaN, 0, false),
						new PayloadTransitRouteStop("pt3", 240, Double.NaN, false)
					},
					null, // in Tramola 23.2.0, changes contain no links
					new PayloadTransitDeparture[]{
						new PayloadTransitDeparture("d99", 7 * 3600, null, null),
						new PayloadTransitDeparture("d98", 8 * 3600, null, null),
						new PayloadTransitDeparture("d97", 9 * 3600, null, null)
					}
				)
			)))
		);

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(1, schedule.getTransitLines().size());
		Assert.assertEquals(3, schedule.getFacilities().size());
		Assert.assertEquals(2, schedule.getTransitLines().get(Id.create("blue", TransitLine.class)).getRoutes().size());

		Assert.assertNotNull(getRoute(schedule, "blue", "b1to3"));
		Assert.assertNotNull(getRoute(schedule, "blue", "b1direct3"));

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1to3").getDepartures().size());
		Assert.assertEquals(3, getRoute(schedule, "blue", "b1direct3").getDepartures().size());

		Assert.assertEquals("pt11", getRoute(schedule, "blue", "b1to3").getRoute().getStartLinkId().toString());
		Assert.assertEquals(3, getRoute(schedule, "blue", "b1to3").getRoute().getLinkIds().size());
		Assert.assertEquals("pt33", getRoute(schedule, "blue", "b1to3").getRoute().getEndLinkId().toString());
	}

	@Test
	public void testApplyChanges_addTransitRouteAndRespectLinks() {
		Scenario scenario = createScenarioWithBusLine();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1").getDepartures().size());

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test", new Change("transitSchedule", new AddTransitRoutePayload("blue", "b1direct3",
				new PayloadTransitRoute("blue", "scenicRoute", "bus", null,
					new PayloadTransitRouteStop[]{
						new PayloadTransitRouteStop("pt1", Double.NaN, 0, false),
						new PayloadTransitRouteStop("pt2", 240, Double.NaN, false)
					},
					new String[]{"12","25","56","36","34"},
					new PayloadTransitDeparture[]{
						new PayloadTransitDeparture("d99", 7 * 3600, null, null),
						new PayloadTransitDeparture("d98", 8 * 3600, null, null),
						new PayloadTransitDeparture("d97", 9 * 3600, null, null)
					}
				)
			)))
		);

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(1, schedule.getTransitLines().size());
		Assert.assertEquals(2, schedule.getFacilities().size());
		Assert.assertEquals(2, schedule.getTransitLines().get(Id.create("blue", TransitLine.class)).getRoutes().size());

		Assert.assertNotNull(getRoute(schedule, "blue", "b1"));
		Assert.assertNotNull(getRoute(schedule, "blue", "scenicRoute"));

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1").getDepartures().size());
		Assert.assertEquals(3, getRoute(schedule, "blue", "scenicRoute").getDepartures().size());

		Assert.assertEquals("12", getRoute(schedule, "blue", "scenicRoute").getRoute().getStartLinkId().toString());
		Assert.assertEquals(3, getRoute(schedule, "blue", "scenicRoute").getRoute().getLinkIds().size());
		Assert.assertEquals("25", getRoute(schedule, "blue", "scenicRoute").getRoute().getLinkIds().get(0).toString());
		Assert.assertEquals("56", getRoute(schedule, "blue", "scenicRoute").getRoute().getLinkIds().get(1).toString());
		Assert.assertEquals("36", getRoute(schedule, "blue", "scenicRoute").getRoute().getLinkIds().get(2).toString());
		Assert.assertEquals("34", getRoute(schedule, "blue", "scenicRoute").getRoute().getEndLinkId().toString());
	}

	@Test
	public void testApplyChanges_addTransitRouteAndFixesLinksIfNeeded() {
		Scenario scenario = createScenarioWithBusLine();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1").getDepartures().size());

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test", new Change("transitSchedule", new AddTransitRoutePayload("blue", "b1direct3",
				new PayloadTransitRoute("blue", "scenicRoute", "bus", null,
					new PayloadTransitRouteStop[]{
						new PayloadTransitRouteStop("pt1", Double.NaN, 0, false),
						new PayloadTransitRouteStop("pt2", 240, Double.NaN, false)
					},
					new String[]{"12","25","36","34"},
					new PayloadTransitDeparture[]{
						new PayloadTransitDeparture("d99", 7 * 3600, null, null),
						new PayloadTransitDeparture("d98", 8 * 3600, null, null),
						new PayloadTransitDeparture("d97", 9 * 3600, null, null)
					}
				)
			)))
		);

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(1, schedule.getTransitLines().size());
		Assert.assertEquals(2, schedule.getFacilities().size());
		Assert.assertEquals(2, schedule.getTransitLines().get(Id.create("blue", TransitLine.class)).getRoutes().size());

		Assert.assertNotNull(getRoute(schedule, "blue", "b1"));
		Assert.assertNotNull(getRoute(schedule, "blue", "scenicRoute"));

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1").getDepartures().size());
		Assert.assertEquals(3, getRoute(schedule, "blue", "scenicRoute").getDepartures().size());

		Assert.assertEquals("12", getRoute(schedule, "blue", "scenicRoute").getRoute().getStartLinkId().toString());
		Assert.assertEquals(3, getRoute(schedule, "blue", "scenicRoute").getRoute().getLinkIds().size());
		Assert.assertEquals("25", getRoute(schedule, "blue", "scenicRoute").getRoute().getLinkIds().get(0).toString());
		Assert.assertEquals("56", getRoute(schedule, "blue", "scenicRoute").getRoute().getLinkIds().get(1).toString());
		Assert.assertEquals("36", getRoute(schedule, "blue", "scenicRoute").getRoute().getLinkIds().get(2).toString());
		Assert.assertEquals("34", getRoute(schedule, "blue", "scenicRoute").getRoute().getEndLinkId().toString());
	}

	@Test
	public void testApplyChanges_updateTransitRouteAndRespectLinks() {
		Scenario scenario = createScenarioWithBusLine();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Assert.assertEquals(10, getRoute(schedule, "blue", "b1").getDepartures().size());

		Assert.assertFalse(getRoute(schedule, "blue", "b1").getStops().get(0).isAwaitDepartureTime());
		Assert.assertFalse(getRoute(schedule, "blue", "b1").getStops().get(1).isAwaitDepartureTime());

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("test", new Change("transitSchedule", new UpdateTransitRoutePayload("blue", "b1",
				new PayloadTransitRoute("blue", "b1", "bus", null,
					new PayloadTransitRouteStop[]{
						new PayloadTransitRouteStop("pt1", Double.NaN, 0, false),
						new PayloadTransitRouteStop("pt2", 240, Double.NaN, true)
					},
					new String[]{"12","25","56","36","34"},
					new PayloadTransitDeparture[]{
						new PayloadTransitDeparture("d99", 7 * 3600, null, null),
						new PayloadTransitDeparture("d98", 8 * 3600, null, null),
						new PayloadTransitDeparture("d97", 9 * 3600, null, null)
					}
				)
			)))
		);

		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(1, schedule.getTransitLines().size());
		Assert.assertEquals(2, schedule.getFacilities().size());
		Assert.assertEquals(1, schedule.getTransitLines().get(Id.create("blue", TransitLine.class)).getRoutes().size());

		Assert.assertNotNull(getRoute(schedule, "blue", "b1"));

		Assert.assertEquals(3, getRoute(schedule, "blue", "b1").getDepartures().size());
		Assert.assertFalse(getRoute(schedule, "blue", "b1").getStops().get(0).isAwaitDepartureTime());
		Assert.assertTrue(getRoute(schedule, "blue", "b1").getStops().get(1).isAwaitDepartureTime());

		Assert.assertEquals("12", getRoute(schedule, "blue", "b1").getRoute().getStartLinkId().toString());
		Assert.assertEquals(3, getRoute(schedule, "blue", "b1").getRoute().getLinkIds().size());
		Assert.assertEquals("25", getRoute(schedule, "blue", "b1").getRoute().getLinkIds().get(0).toString());
		Assert.assertEquals("56", getRoute(schedule, "blue", "b1").getRoute().getLinkIds().get(1).toString());
		Assert.assertEquals("36", getRoute(schedule, "blue", "b1").getRoute().getLinkIds().get(2).toString());
		Assert.assertEquals("34", getRoute(schedule, "blue", "b1").getRoute().getEndLinkId().toString());
	}

	@Test
	public void testApplyChanges_deleteTransitRouteStop() {
		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Assert.assertEquals(3, getRoute(schedule, "blue", "b1to3").getStops().size());

		List<Changeset> changesets = new ArrayList<>();
		changesets.add(
			new Changeset("deleteRouteStop",
				new Change("transitSchedule",
					new DeleteTransitRouteStopPayload("blue", "b1to3", "pt2", 1)
				)
			)
		);

		Assert.assertEquals(3, schedule.getFacilities().size());
		ChangesetApplier applier = new ChangesetApplier(scenario, new IdentityTransformation());
		applier.applyChanges(changesets);

		Assert.assertEquals(1, schedule.getTransitLines().size());
		Assert.assertEquals(2, schedule.getFacilities().size());
		Assert.assertEquals(2, getRoute(schedule, "blue", "b1to3").getStops().size());
	}

	@Test
	public void testApplyChanges_network() {

		Scenario scenario = createScenario();
		Network network = scenario.getNetwork();

		Assert.assertEquals(4, network.getNodes().size());
		Assert.assertEquals(7, network.getLinks().size());

		Changeset cs = new Changeset("test",
			new Change("test", new AddNodePayload("n5", 1, 2)),
			new Change("test", new AddNodePayload("n6", 5, 6)),
			new Change("test", new AddLinkPayload("l6", "n5", "n6", Map.of("freespeed", 15)))
		);

		Changeset.apply(scenario, cs);

		Assert.assertEquals(6, network.getNodes().size());
		Assert.assertEquals(8, network.getLinks().size());


		Assert.assertEquals(5, network.getNodes().get(Id.createNodeId("n6")).getCoord().getX(), 0);

	}

	@Test
	public void testApplyChanges_networkModifications() {

		Scenario scenario = createScenario();
		Network network = scenario.getNetwork();

		Changeset cs = new Changeset("test",
			new Change("test", new DeleteNodePayload("1")),
			new Change("test", new DeleteLinkPayload("12")),
			new Change("test", new UpdateLinkPayload("23", Map.of("freespeed", 15)))
		);

		Changeset.apply(scenario, cs);

		Assert.assertNull(network.getNodes().get(Id.createNodeId("1")));
		Assert.assertNull(network.getLinks().get(Id.createLinkId("12")));

		Link link = network.getLinks().get(Id.createLinkId("23"));

		Assert.assertEquals(15, link.getFreespeed(), 0);
	}

	@Test
	public void affect_ptNetwork() {

		Scenario scenario = createScenario();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Changeset cs = new Changeset("test", new Change("test", new NetworkGroupPayload("modify",
			new Change("test", new DeleteLinkPayload("pt22")),
			new Change("test", new AddNodePayload("n", 0, 0)),
			new Change("test", new AddLinkPayload("pt221", "2", "n", Map.of())),
			new Change("test", new AddLinkPayload("pt222", "n", "2", Map.of()))
		)));

		Changeset.apply(scenario, cs);

		TransitLine line = schedule.getTransitLines().get(Id.create("blue", TransitLine.class));
		TransitRoute route = line.getRoutes().get(Id.create("b1to3", TransitRoute.class));

		Assert.assertEquals(
			List.of(Id.createLinkId("12"), Id.createLinkId("pt221"), Id.createLinkId("pt222"), Id.createLinkId("23")),
			route.getRoute().getLinkIds());

	}

	@Test
	public void testApplyChanges_transitNetworkRoute() {

		Scenario scenario = createScenarioWithBusLine();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Changeset cs = new Changeset("test",
			new Change("test", new DeleteTransitNetworkRouteLinkPayload("blue", "b1", "23", 1)),
			new Change("test", new DeleteTransitNetworkRouteLinkPayload("blue", "b1", "12", 1)),
			new Change("test", new DeleteTransitNetworkRouteLinkPayload("blue", "b1", "34", 1)),
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "", 1, "34", true)),
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "34", 1, "12", true)),
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "12", 1, "25", false)),
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "25", 1, "56", false)),
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "56", 1, "36", false))
		);

		Changeset.apply(scenario, cs);

		TransitLine line = schedule.getTransitLines().get(Id.create("blue", TransitLine.class));
		TransitRoute route = line.getRoutes().get(Id.create("b1", TransitRoute.class));

		Assert.assertEquals(Id.create("12", Link.class), route.getRoute().getStartLinkId());
		Assert.assertEquals(Id.create("25", Link.class), route.getRoute().getLinkIds().get(0));
		Assert.assertEquals(Id.create("56", Link.class), route.getRoute().getLinkIds().get(1));
		Assert.assertEquals(Id.create("36", Link.class), route.getRoute().getLinkIds().get(2));
		Assert.assertEquals(Id.create("34", Link.class), route.getRoute().getEndLinkId());
	}


	@Test
	public void testApplyChanges_transitNetworkRouteLoop() {

		Scenario scenario = createScenarioWithBusLine();
		TransitSchedule schedule = scenario.getTransitSchedule();

		Changeset cs = new Changeset("test",

			// loop outer loop first time
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "12", 1, "25", false)),
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "25", 1, "56", false)),
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "56", 1, "36", false)),
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "36", 1, "23r", false)),

			// loop outer loop second time
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "12", 1, "25", false)),
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "25", 1, "56", false)),
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "56", 1, "36", false)),
			new Change("test", new AddTransitNetworkRouteLinkPayload("blue", "b1", "36", 1, "23r", false)),

			// delete second loop
			new Change("test", new DeleteTransitNetworkRouteLinkPayload("blue", "b1", "25", 2)),
			new Change("test", new DeleteTransitNetworkRouteLinkPayload("blue", "b1", "56", 2)),
			new Change("test", new DeleteTransitNetworkRouteLinkPayload("blue", "b1", "36", 2))
			);

		Changeset.apply(scenario, cs);

		TransitLine line = schedule.getTransitLines().get(Id.create("blue", TransitLine.class));
		TransitRoute route = line.getRoutes().get(Id.create("b1", TransitRoute.class));

		Assert.assertEquals(Id.create("12", Link.class), route.getRoute().getStartLinkId());
		Assert.assertEquals(Id.create("25", Link.class), route.getRoute().getLinkIds().get(0));
		Assert.assertEquals(Id.create("56", Link.class), route.getRoute().getLinkIds().get(1));
		Assert.assertEquals(Id.create("36", Link.class), route.getRoute().getLinkIds().get(2));
		Assert.assertEquals(Id.create("23r", Link.class), route.getRoute().getLinkIds().get(3));
		// gap - rerouting to fix route should choose the shortest path -> link 23
		Assert.assertEquals(Id.create("23", Link.class), route.getRoute().getLinkIds().get(4));
		Assert.assertEquals(Id.create("34", Link.class), route.getRoute().getEndLinkId());
	}

    private TransitRoute getRoute(TransitSchedule schedule, String lineId, String routeId) {
        return schedule.getTransitLines().get(Id.create(lineId, TransitLine.class)).getRoutes().get(Id.create(routeId, TransitRoute.class));
    }

	private Scenario createScenario() {

		/* Creates the following test network:
		 *
		 *    (4) <---- 34 ---- (3)
		 *     |                 ^
		 *     |                 |
		 *    41                 23
		 *     |                 |
		 *     v                 |
		 *    (1) ---- 12 ----> (2)
		 */

		Scenario scenario = ScenarioUtils.createScenario(ConfigUtils.createConfig());

		Network network = scenario.getNetwork();
		NetworkFactory nf = network.getFactory();

		Node node1 = nf.createNode(Id.createNodeId("1"), new Coord(0, 0));
		Node node2 = nf.createNode(Id.createNodeId("2"), new Coord(1000, 0));
		Node node3 = nf.createNode(Id.createNodeId("3"), new Coord(1000, 1000));
		Node node4 = nf.createNode(Id.createNodeId("4"), new Coord(0, 1000));

		network.addNode(node1);
		network.addNode(node2);
		network.addNode(node3);
		network.addNode(node4);

		addLink(network, "12", node1, node2, "car", 10.0, 2000.0, 1000.0, 1.0);
		addLink(network, "23", node2, node3, "car", 10.0, 2000.0, 1000.0, 1.0);
		addLink(network, "34", node3, node4, "car", 10.0, 2000.0, 1000.0, 1.0);
		addLink(network, "41", node4, node1, "car", 10.0, 2000.0, 1000.0, 1.0);

		createSchedule(scenario);

		return scenario;
	}

	private Scenario createScenarioWithBusLine() {

		/* Creates the following test network:                 And pt route:
		 *					                                            			(Bedorf)
		 *    (4) <---- 34 ---- (3) <---- 36 --- (6)           (4) <---- 34 ---- (3)
		 *     |                 ^               ^                                ^
		 *     |                 |    |          |                                |
		 *    41                 23  23r         56                               23
		 *     |                 |    v          |                                |
		 *     v                 |               |                                |
		 *    (1) ---- 12 ----> (2) ---- 25 ---> (5)           (1) ---- 12 ----> (2)
		 * 					                                          			(Aweiler)
		 */

		Scenario scenario = ScenarioUtils.createScenario(ConfigUtils.createConfig());

		Network network = scenario.getNetwork();
		NetworkFactory nf = network.getFactory();

		Node node1 = nf.createNode(Id.createNodeId("1"), new Coord(0, 0));
		Node node2 = nf.createNode(Id.createNodeId("2"), new Coord(1000, 0));
		Node node3 = nf.createNode(Id.createNodeId("3"), new Coord(1000, 1000));
		Node node4 = nf.createNode(Id.createNodeId("4"), new Coord(0, 1000));
		Node node5 = nf.createNode(Id.createNodeId("5"), new Coord(0, 2000));
		Node node6 = nf.createNode(Id.createNodeId("6"), new Coord(1000, 2000));

		network.addNode(node1);
		network.addNode(node2);
		network.addNode(node3);
		network.addNode(node4);
		network.addNode(node5);
		network.addNode(node6);

		Link l12 = addLink(network, "12", node1, node2, "car", 10.0, 2000.0, 1000.0, 1.0);
		Link l23 = addLink(network, "23", node2, node3, "car", 10.0, 2000.0, 1000.0, 1.0);
		Link l23r = addLink(network, "23r", node3, node2, "car", 10.0, 2000.0, 1000.0, 1.0);
		Link l34 = addLink(network, "34", node3, node4, "car", 10.0, 2000.0, 1000.0, 1.0);
		Link l41 = addLink(network, "41", node4, node1, "car", 10.0, 2000.0, 1000.0, 1.0);
		Link l25 = addLink(network, "25", node2, node5, "car", 10.0, 2000.0, 1000.0, 1.0);
		Link l56 = addLink(network, "56", node5, node6, "car", 10.0, 2000.0, 1000.0, 1.0);
		Link l36 = addLink(network, "36", node6, node3, "car", 10.0, 2000.0, 1000.0, 1.0);

		TransitSchedule schedule = scenario.getTransitSchedule();

		TransitScheduleFactory tf = schedule.getFactory();

		TransitStopFacility stop1 = tf.createTransitStopFacility(Id.create("pt1", TransitStopFacility.class), new Coord(0, 0), false);
		stop1.setLinkId(l12.getId());
		stop1.setName("Aweiler");

		TransitStopFacility stop2 = tf.createTransitStopFacility(Id.create("pt2", TransitStopFacility.class), new Coord(0, 1000), false);
		stop2.setLinkId(l34.getId());
		stop2.setName("Bedorf");

		schedule.addStopFacility(stop1);
		schedule.addStopFacility(stop2);

		TransitLine blueLine = tf.createTransitLine(Id.create("blue", TransitLine.class));

		NetworkRoute networkRoute = RouteUtils.createLinkNetworkRouteImpl(l12.getId(), List.of(l23.getId()), l34.getId());
		List<TransitRouteStop> stops = List.of(
			tf.createTransitRouteStop(stop1, OptionalTime.undefined(), OptionalTime.defined(0)),
			tf.createTransitRouteStop(stop2, OptionalTime.undefined(), OptionalTime.defined(150))
		);

		TransitRoute blue1 = tf.createTransitRoute(Id.create("b1", TransitRoute.class), networkRoute, stops, "bus");
		for (int i = 0; i < 10; i++) {
			Departure d = tf.createDeparture(Id.create("d" + i, Departure.class), 6 * 3600 + i * 1800);
			d.setVehicleId(Id.create("v" + i, Vehicle.class));

			blue1.addDeparture(d);
		}
		blueLine.addRoute(blue1);
		schedule.addTransitLine(blueLine);

		return scenario;
	}

	private Link addLink(Network network, String id, Node n1, Node n2, String mode,
											 double freespeed, double capacity, double length, double lanes) {

		Link link = network.getFactory().createLink(Id.createLinkId(id), n1, n2);
		link.setAllowedModes(Set.of(mode));
		link.setFreespeed(freespeed);
		link.setCapacity(capacity);
		link.setLength(length);
		link.setNumberOfLanes(lanes);

		network.addLink(link);

		return link;
	}

	private TransitSchedule createSchedule(Scenario scenario) {

		TransitSchedule schedule = scenario.getTransitSchedule();
		Network network = scenario.getNetwork();


		/* Creates the following test schedule:
		 *
		 *                   (pt3)
		 *                     ^
		 *                     |
		 *                     |
		 *                     |
		 *  (pt1)----------->(pt2)
		 */


		Node node1 = network.getNodes().get(Id.createNodeId("1"));
		Node node2 = network.getNodes().get(Id.createNodeId("2"));
		Node node3 = network.getNodes().get(Id.createNodeId("3"));
		Link link12 = network.getLinks().get(Id.createLinkId("12"));
		Link link23 = network.getLinks().get(Id.createLinkId("23"));

		Link link11 = addLink(network, "pt11", node1, node1, "car", 10, 2000, 50, 2);
		Link link22 = addLink(network, "pt22", node2, node2, "car", 10.0, 2000.0, 50.0, 2.0);
		Link link33 = addLink(network, "pt33", node3, node3, "car", 10.0, 2000.0, 50.0, 2.0);

		TransitScheduleFactory tf = schedule.getFactory();

		TransitStopFacility stop1 = tf.createTransitStopFacility(Id.create("pt1", TransitStopFacility.class), new Coord(0, 0), false);
		stop1.setLinkId(link11.getId());
		stop1.setName("Aweiler");

		TransitStopFacility stop2 = tf.createTransitStopFacility(Id.create("pt2", TransitStopFacility.class), new Coord(1000, 0), false);
		stop2.setLinkId(link22.getId());
		stop2.setName("Bedorf");

		TransitStopFacility stop3 = tf.createTransitStopFacility(Id.create("pt3", TransitStopFacility.class), new Coord(1000, 1000), false);
		stop3.setLinkId(link33.getId());
		stop3.setName("Cestadt");

		schedule.addStopFacility(stop1);
		schedule.addStopFacility(stop2);
		schedule.addStopFacility(stop3);

		TransitLine blueLine = tf.createTransitLine(Id.create("blue", TransitLine.class));

		NetworkRoute networkRoute = RouteUtils.createLinkNetworkRouteImpl(link11.getId(), List.of(link12.getId(), link22.getId(), link23.getId()), link33.getId());
		List<TransitRouteStop> stops = List.of(
			tf.createTransitRouteStop(stop1, OptionalTime.undefined(), OptionalTime.defined(0)),
			tf.createTransitRouteStop(stop2, OptionalTime.undefined(), OptionalTime.defined(150)),
			tf.createTransitRouteStop(stop3, OptionalTime.defined(280), OptionalTime.undefined())
		);

		TransitRoute blue1to3 = tf.createTransitRoute(Id.create("b1to3", TransitRoute.class), networkRoute, stops, "bus");
		for (int i = 0; i < 10; i++) {
			Departure d = tf.createDeparture(Id.create("d" + i, Departure.class), 6 * 3600 + i * 1800);
			d.setVehicleId(Id.create("v" + i, Vehicle.class));

			blue1to3.addDeparture(d);
		}
		blueLine.addRoute(blue1to3);
		schedule.addTransitLine(blueLine);

		return schedule;
	}

}
