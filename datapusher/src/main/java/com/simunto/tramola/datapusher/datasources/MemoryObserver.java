package com.simunto.tramola.datapusher.datasources;

import com.simunto.tramola.datapusher.Reading;
import com.simunto.tramola.datapusher.TramolaClient;
import com.simunto.tramola.datapusher.Serie;

/**
 * @author mrieser / Simunto
 */
public final class MemoryObserver {

	private final TramolaClient client;

	public MemoryObserver(TramolaClient client) {
		this.client = client;
	}

	public void start(int interval_seconds) {
		Serie memorySerie = this.client.getRegisteredRun().addSerie("memory", Serie.SerieType.LONG);
		Thread t = new Thread(() -> {
			try {
				while (true) {
					long totalMem = Runtime.getRuntime().totalMemory();
					long freeMem = Runtime.getRuntime().freeMemory();
					long usedMem = totalMem - freeMem;
					memorySerie.addReading(Reading.build()
							.value("used", usedMem)
							.value("free", freeMem)
							.value("total", totalMem)
							.get());
					Thread.sleep(interval_seconds * 1000L);
				}
			} catch (InterruptedException e) {
				System.err.println("MemoryObserver got interrupted, shutting down now.");
				e.printStackTrace();
			}
		}, "MemoryObserver");
		t.setDaemon(true);
		t.start();
	}

}
