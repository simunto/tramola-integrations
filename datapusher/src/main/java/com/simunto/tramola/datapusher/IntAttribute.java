package com.simunto.tramola.datapusher;

/**
 * @author mrieser
 */
class IntAttribute extends Attribute {

	final int value;

	IntAttribute(String name, int value) {
		super(name);
		this.value = value;
	}
}
