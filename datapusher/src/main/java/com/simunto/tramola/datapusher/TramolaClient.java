package com.simunto.tramola.datapusher;

/**
 * Client interface to interact with Tramola Server.
 *
 * Each client can manage one Run.
 *
 * @author mrieser
 */
public class TramolaClient {

	private final String hostUrl;
	private final String apikey;
	private final String password;
	private String group = null;

	private Run run = null;

	public TramolaClient(String hostUrl, String apikey, String password) {
		this.hostUrl = createUrl(hostUrl);
		this.apikey = apikey;
		this.password = password;
	}

	private static String createUrl(String hostUrl) {
		// Check if running inside a docker container
		if ("true".equalsIgnoreCase(System.getenv("TRAMOLA_DOCKER"))) {
			// localhost is replaced with the docker host
			return hostUrl.replace("localhost", "host.docker.internal");
		}

		return hostUrl;
	}

	public String getName() {
		return this.hostUrl;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public Run registerRun(Run run) {
		if (this.run != null) {
			throw new IllegalStateException("There is already a run registered.");
		}
		this.run = run;
		try {
			run.register(this.hostUrl, this.apikey, this.password, this.group);
		} catch (Exception e) {
			this.run = new NoopRun();
			throw e;
		}
		return run;
	}

	public Run getRegisteredRun() {
		return this.run;
	}
}
