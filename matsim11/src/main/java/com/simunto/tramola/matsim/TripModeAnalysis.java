package com.simunto.tramola.matsim;

import com.simunto.tramola.datapusher.Reading;
import com.simunto.tramola.datapusher.Run;
import com.simunto.tramola.datapusher.Serie;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.router.MainModeIdentifier;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.TripRouter;
import org.matsim.core.router.TripStructureUtils;
import org.matsim.core.router.TripStructureUtils.Trip;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author mrieser / Simunto GmbH
 */
public class TripModeAnalysis implements IterationEndsListener {

	private final Scenario scenario;
	private final TripRouter tripRouter;
	private final Serie modeStats;

	public TripModeAnalysis(Scenario scenario, TripRouter tripRouter, Run run) {
		this.scenario = scenario;
		this.tripRouter = tripRouter;
		this.modeStats = run.addSerie("modeStats", Serie.SerieType.LONG);
	}

	@Override
	public void notifyIterationEnds(IterationEndsEvent event) {
		Population population = this.scenario.getPopulation();
		StageActivityTypes stageActivities = this.tripRouter.getStageActivityTypes();
		MainModeIdentifier mainModeIdentifier = this.tripRouter.getMainModeIdentifier();
		Map<String, Integer> modeCnt = new TreeMap<>();

		for (Person person : population.getPersons().values()) {
			Plan plan = person.getSelectedPlan() ;
			List<Trip> trips = TripStructureUtils.getTrips(plan, stageActivities);
			for (Trip trip : trips) {
				String mode = mainModeIdentifier.identifyMainMode(trip.getTripElements());
				modeCnt.compute(mode, (key, old) -> old == null ? 1 : old + 1);
			}
		}

		Reading.ReadingBuilder reading = Reading.build().iteration(event.getIteration());
		for (Map.Entry<String, Integer> e : modeCnt.entrySet()) {
			reading.value(e.getKey(), e.getValue());
		}
		this.modeStats.addReading(reading.get());
	}

}
