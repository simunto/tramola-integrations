package com.simunto.tramola.datapusher;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

/**
 * @author mrieser / Simunto GmbH
 */
class NoopRun extends Run {

	public NoopRun() {
		super(0, "unregistered run", Timer.currentTimeMillis(), Collections.emptyList());
	}

	@Override
	public Serie addSerie(String serieName, Serie.SerieType type) {
		return new NoopSerie(this, serieName);
	}

	@Override
	void addReading(Serie series, Reading reading) {
	}

	@Override
	public void addFile(File file) {
	}

	@Override
	public void importCSV(String serieName, File file) throws IOException {
	}

	@Override
	public void importCSV(String serieName, File file, String iterationColumnName) throws IOException {
	}

	@Override
	public void importTabSeparated(String serieName, File file) throws IOException {
	}

	@Override
	public void importTabSeparated(String serieName, File file, String iterationColumnName) throws IOException {
	}

	@Override
	public void addEvent(String type, String data) {
	}

	@Override
	public void close() {
	}
}
