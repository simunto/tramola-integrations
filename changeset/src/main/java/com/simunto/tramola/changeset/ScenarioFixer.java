package com.simunto.tramola.changeset;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.network.Node;
import org.matsim.core.population.routes.NetworkRoute;
import org.matsim.core.population.routes.RouteUtils;
import org.matsim.core.router.costcalculators.OnlyTimeDependentTravelDisutility;
import org.matsim.core.router.speedy.SpeedyDijkstra;
import org.matsim.core.router.speedy.SpeedyGraph;
import org.matsim.core.router.util.LeastCostPathCalculator.Path;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.trafficmonitoring.FreeSpeedTravelTime;
import org.matsim.core.utils.geometry.CoordUtils;
import org.matsim.pt.transitSchedule.api.Departure;
import org.matsim.pt.transitSchedule.api.TransitLine;
import org.matsim.pt.transitSchedule.api.TransitRoute;
import org.matsim.pt.transitSchedule.api.TransitRouteStop;
import org.matsim.pt.transitSchedule.api.TransitScheduleFactory;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;
import org.matsim.vehicles.Vehicle;
import org.matsim.vehicles.VehicleType;
import org.matsim.vehicles.VehicleUtils;
import org.matsim.vehicles.Vehicles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Tries to make sure the scenario data is valid, e.g.:
 * <ul>
 *   <li>If transit routes are not serving certain stops, add custom links to fix it.</li>
 * </ul>
 */
public class ScenarioFixer {

	private final Scenario scenario;

	public ScenarioFixer(Scenario scenario) {
		this.scenario = scenario;
	}

	public void fixTransitSchedule() {
		SpeedyGraph graph = new SpeedyGraph(this.scenario.getNetwork());
		TravelTime travelTime = new FreeSpeedTravelTime();
		SpeedyDijkstra dijkstra = new SpeedyDijkstra(graph, travelTime, new OnlyTimeDependentTravelDisutility(travelTime));
		for (TransitLine line : this.scenario.getTransitSchedule().getTransitLines().values()) {
			for (TransitRoute route : line.getRoutes().values()) {
				fixMissingLinks(route, dijkstra);
				fixTransitRouteNetworkRoute(route);
				fixTransitRouteVehicles(line, route);
			}
		}
		removeEmptyStopFacilities();
	}

	private void fixMissingLinks(TransitRoute route, SpeedyDijkstra dijkstra) {
		NetworkRoute networkRoute = route.getRoute();
		List<Id<Link>> routeLinkIds = new ArrayList<>();
		if (networkRoute != null) {
			routeLinkIds.add(networkRoute.getStartLinkId());
			routeLinkIds.addAll(networkRoute.getLinkIds());
			routeLinkIds.add(networkRoute.getEndLinkId());
		}

		// remove links from network route that no longer exist
		Network network = this.scenario.getNetwork();
		Iterator<Id<Link>> iter = routeLinkIds.listIterator();
		while (iter.hasNext()) {
			Id<Link> linkId = iter.next();
			Link link = network.getLinks().get(linkId);
			if (link == null) {
				iter.remove();
			}
		}

		// check all stop links still exist
		for (TransitRouteStop stop : route.getStops()) {
			Id<Link> linkId = stop.getStopFacility().getLinkId();
			Link link = network.getLinks().get(linkId);
			if (link == null) {
				stop.getStopFacility().setLinkId(null);
			}
		}

		// check that network route has no gaps, fill it up with dijkstra-search
		Link prevLink = null;
		List<Id<Link>> fixedRouteLinkIds = new ArrayList<>();
		for (Id<Link> linkId : routeLinkIds) {
			Link link = network.getLinks().get(linkId);
			if (prevLink != null) {
				if (prevLink.getToNode() != link.getFromNode()) {
					Path path = dijkstra.calcLeastCostPath(prevLink.getToNode(), link.getFromNode(), 8*3600, null, null);
					if (path != null) {
						for (Link pathLink : path.links) {
							fixedRouteLinkIds.add(pathLink.getId());
						}
					}
				}
			}
			fixedRouteLinkIds.add(link.getId());
			prevLink = link;
		}

		int linkCount = fixedRouteLinkIds.size();
		if (linkCount > 2) {
			route.setRoute(RouteUtils.createLinkNetworkRouteImpl(fixedRouteLinkIds.get(0), fixedRouteLinkIds.subList(1, linkCount - 1), fixedRouteLinkIds.get(linkCount - 1)));
		} else if (linkCount > 0) {
			route.setRoute(RouteUtils.createLinkNetworkRouteImpl(fixedRouteLinkIds.get(0), Collections.emptyList(), fixedRouteLinkIds.get(linkCount - 1)));
		}
	}

	private void fixTransitRouteNetworkRoute(TransitRoute route) {
		NetworkRoute networkRoute = route.getRoute();
		List<Id<Link>> routeLinkIds = new ArrayList<>();
		if (networkRoute != null) {
			routeLinkIds.add(networkRoute.getStartLinkId());
			routeLinkIds.addAll(networkRoute.getLinkIds());
			routeLinkIds.add(networkRoute.getEndLinkId());
		}
//		Id<Vehicle> vehicleId = networkRoute == null ? null : networkRoute.getVehicleId();
		List<TransitRouteStop> stops = route.getStops();

		Network network = this.scenario.getNetwork();
		boolean[] stopOnRoute = new boolean[stops.size()];
		int linkPosition = 0;
		for (int stopIdx = 0; stopIdx < stops.size(); stopIdx++) {
			TransitRouteStop routeStop = stops.get(stopIdx);
			TransitStopFacility stopFacility = routeStop.getStopFacility();
			Id<Link> linkId = stopFacility.getLinkId();
			if (linkId == null) {
				// probably a new stop facility, create a loop-link for it
				Coord coord = stopFacility.getCoord();
				Node node = network.getFactory().createNode(Id.create("tramolaStop_" + stopFacility.getId(), Node.class), new Coord(coord.getX(), coord.getY()));
				network.addNode(node);
				linkId = Id.create("tramolaStop_" + stopFacility.getId().toString(), Link.class);
				Link link = network.getFactory().createLink(linkId, node, node);
				link.setAllowedModes(Set.of("pt", route.getTransportMode()));
				link.setFreespeed(20.0);
				link.setCapacity(2000.0);
				link.setLength(20.0);
				link.setNumberOfLanes(2.0);
				this.scenario.getNetwork().addLink(link);

				stopFacility.setLinkId(linkId);
			}
			// search in remaining route links for the stop-link
			for (int pos = linkPosition; pos < routeLinkIds.size(); pos++) {
				if (linkId.equals(routeLinkIds.get(pos))) {
					linkPosition = pos;
					stopOnRoute[stopIdx] = true;
					break;
				}
			}
		}

		List<Id<Link>> fixedRouteLinkIds = new ArrayList<>();
		linkPosition = 0;
		TransitRouteStop prevStop = stops.get(0);
		Id<Link> prevStopLinkId = prevStop.getStopFacility().getLinkId();
		if (stopOnRoute[0]) {
			for (int pos = linkPosition; pos < routeLinkIds.size(); pos++) {
				Id<Link> linkId = routeLinkIds.get(pos);
				if (prevStopLinkId.equals(linkId)) {
					fixedRouteLinkIds.add(linkId);
					linkPosition = pos;
					break;
				}
				fixedRouteLinkIds.add(linkId);
			}
		} else {
			fixedRouteLinkIds.add(prevStopLinkId);
		}
		for (int stopIdx = 1; stopIdx < stops.size(); stopIdx++) {
			TransitRouteStop stop = stops.get(stopIdx);
			Id<Link> stopLinkId = stop.getStopFacility().getLinkId();
			boolean needFixedRoute = (!stopOnRoute[stopIdx - 1] || !stopOnRoute[stopIdx]);
			boolean isOnSameLink = prevStopLinkId.equals(stopLinkId);
			if (!isOnSameLink) {
				if (needFixedRoute) {
					Link directLink = findOrCreateDirectLink(prevStopLinkId, stopLinkId, route.getTransportMode());
					fixedRouteLinkIds.add(directLink.getId());
					fixedRouteLinkIds.add(stopLinkId);
					if (stopOnRoute[stopIdx]) {
						// move linkPosition to point to the right place again
						for (int pos = linkPosition; pos < routeLinkIds.size(); pos++) {
							Id<Link> linkId = routeLinkIds.get(pos);
							if (stopLinkId.equals(linkId)) {
								linkPosition = pos;
								break;
							}
						}
					}
				} else {
					// copy over links from route
					for (int pos = linkPosition; pos < routeLinkIds.size(); pos++) {
						Id<Link> linkId = routeLinkIds.get(pos);
						if (stopLinkId.equals(linkId)) {
							if (pos != linkPosition) {
								fixedRouteLinkIds.add(linkId);
							}
							linkPosition = pos;
							break;
						}
						if (pos > linkPosition) {
							fixedRouteLinkIds.add(linkId);
						}
					}
				}
			}
			prevStop = stop;
			prevStopLinkId = stopLinkId;
		}

		int linkCount = fixedRouteLinkIds.size();
		if (linkCount > 2) {
			route.setRoute(RouteUtils.createLinkNetworkRouteImpl(fixedRouteLinkIds.get(0), fixedRouteLinkIds.subList(1, linkCount - 1), fixedRouteLinkIds.get(linkCount - 1)));
		} else if (linkCount > 0) {
			route.setRoute(RouteUtils.createLinkNetworkRouteImpl(fixedRouteLinkIds.get(0), Collections.emptyList(), fixedRouteLinkIds.get(linkCount - 1)));
		}
	}

	private void fixTransitRouteVehicles(TransitLine line, TransitRoute route) {
		Vehicles vehicles = this.scenario.getTransitVehicles();
		if (vehicles == null) {
			return;
		}
		TransitScheduleFactory ptFactory = this.scenario.getTransitSchedule().getFactory();
		VehicleType vehType = null;
		List<Departure> fixedDepartures = new ArrayList<>();
		for (Departure dep : route.getDepartures().values()) {
			Id<Vehicle> vehicleId = dep.getVehicleId();
			Vehicle veh = vehicleId == null ? null : vehicles.getVehicles().get(vehicleId);
			if (veh == null) {
				if (vehType == null) {
					vehType = findOrCreateVehicleType(line);
				}
				if (vehicleId == null) {
					vehicleId = Id.create("tramolaPTVeh_" + line.getId().toString() + "_" + route.getId() + "_" + dep.getId(), Vehicle.class);
				}
				vehicles.addVehicle(VehicleUtils.createVehicle(vehicleId, vehType));
				Departure fixedDep = ptFactory.createDeparture(dep.getId(), dep.getDepartureTime());
				fixedDep.setVehicleId(vehicleId);
				fixedDepartures.add(fixedDep);
			}
		}
		for (Departure dep : fixedDepartures) {
			route.removeDeparture(dep);
			route.addDeparture(dep);
		}
	}

	private Link findOrCreateDirectLink(Id<Link> fromLinkId, Id<Link> toLinkId, String routeMode) {
		Network network = this.scenario.getNetwork();
		Link fromLink = network.getLinks().get(fromLinkId);
		Link toLink = network.getLinks().get(toLinkId);
		org.matsim.api.core.v01.network.Node fromNode = fromLink.getToNode();
		org.matsim.api.core.v01.network.Node toNode = toLink.getFromNode();
		for (Link candidate : fromNode.getOutLinks().values()) {
			if (candidate.getToNode().equals(toNode)) {
				return candidate;
			}
		}
		// no direct link found, create one
		double length = Math.ceil(1.05 * CoordUtils.calcEuclideanDistance(fromNode.getCoord(), toNode.getCoord()));
		Link directLink = network.getFactory().createLink(
			Id.create("tramolaPT_" + fromNode.getId().toString() + "_" + toNode.getId().toString(), Link.class),
			fromNode,
			toNode
		);
		directLink.setAllowedModes(Set.of("pt", routeMode));
		directLink.setFreespeed(30.0);
		directLink.setCapacity(2000.0);
		directLink.setLength(length);
		directLink.setNumberOfLanes(2.0);
		network.addLink(directLink);
		return directLink;
	}

	private VehicleType findOrCreateVehicleType(TransitLine line) {
		Vehicles vehicles = this.scenario.getTransitVehicles();
		for (TransitRoute route : line.getRoutes().values()) {
			for (Departure dep : route.getDepartures().values()) {
				if (dep.getVehicleId() != null) {
					Vehicle vehicle = vehicles.getVehicles().get(dep.getVehicleId());
					VehicleType vehType = vehicle == null ? null : vehicle.getType();
					if (vehType != null) {
						return vehType;
					}
				}
			}
		}
		Id<VehicleType> fallbackId = Id.create("tramolaPT", VehicleType.class);
		VehicleType vehType = vehicles.getVehicleTypes().get(fallbackId);
		if (vehType == null) {
			vehType = VehicleUtils.createVehicleType(fallbackId);
			vehType.setDescription("Default transit vehicle type generated by Tramola");
			vehType.getCapacity().setSeats(100);
			vehType.getCapacity().setStandingRoom(30);
			vehType.setPcuEquivalents(5);
			vehicles.addVehicleType(vehType);
		}
		return vehType;
	}

	private void removeEmptyStopFacilities() {
		Set<Id<TransitStopFacility>> usedStopIds = new HashSet<>();
		for (TransitLine line : this.scenario.getTransitSchedule().getTransitLines().values()) {
			for (TransitRoute route : line.getRoutes().values()) {
				for (TransitRouteStop stop : route.getStops()) {
					usedStopIds.add(stop.getStopFacility().getId());
				}
			}
		}
		List<TransitStopFacility> stopsToRemove = new ArrayList<>();
		for (TransitStopFacility stopFacility : this.scenario.getTransitSchedule().getFacilities().values()) {
			if (!usedStopIds.contains(stopFacility.getId())) {
				stopsToRemove.add(stopFacility);
			}
		}
		for (TransitStopFacility stop : stopsToRemove) {
			this.scenario.getTransitSchedule().removeStopFacility(stop);
		}
	}

}
