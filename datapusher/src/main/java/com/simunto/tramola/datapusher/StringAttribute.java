package com.simunto.tramola.datapusher;

/**
 * @author mrieser
 */
class StringAttribute extends Attribute {

	final String value;

	StringAttribute(String name, String value) {
		super(name);
		this.value = value;
	}
}
