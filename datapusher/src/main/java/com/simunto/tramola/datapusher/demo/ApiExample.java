package com.simunto.tramola.datapusher.demo;

import com.simunto.tramola.datapusher.Reading;
import com.simunto.tramola.datapusher.Run;
import com.simunto.tramola.datapusher.TramolaClient;
import com.simunto.tramola.datapusher.Serie;

import java.io.File;

/**
 * @author mrieser
 */
public class ApiExample {

	public static void main(String[] args) {
		System.setProperty("tramola.apikey", "Y5PJ-XEYP-ELCL-C2TX");
		System.setProperty("tramola.apipassword", "MCPw4mXqKq");

//		String hostUrl = "https://observatory.senozon.com";
//		String hostUrl = "http://localhost:5381";
		String hostUrl = "http://localhost:5382";
		String apikey = System.getProperty("tramola.apikey");
		String password = System.getProperty("tramola.apipassword");

		TramolaClient mc = new TramolaClient(hostUrl, apikey, password);

		Run run1 = mc.registerRun(Run.build()
				.withName("My First Run")
				.withAttribute("a", "abc")
				.withAttribute("b", 27)
				.withAttribute("c", 23.11)
				.get()
		);

		Serie serie1 = run1.addSerie("modalsplit", Serie.SerieType.DOUBLE);

		serie1.addReading(Reading.build()
				.iteration(10)
				.value("car", 0.1)
				.value("pt", 0.8)
				.value("bike", 0.1)
				.get()
		);

		run1.addFile(new File("output_network.xml.gz"));

		run1.close();
	}
}
