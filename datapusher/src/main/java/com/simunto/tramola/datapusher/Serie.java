package com.simunto.tramola.datapusher;

/**
 * @author mrieser
 */
public class Serie {

    public enum SerieType {
        DOUBLE, LONG, STRING
    }

    private Run run;
    private String name;
    private SerieType type;
    private long id = -1;

    public Serie(Run run, String seriesName, SerieType type) {
        this.run = run;
        this.name = seriesName;
        this.type = type;
    }

    void setId(long id) {
        if (this.id >= 0) {
            throw new IllegalStateException("Serie Id is already set.");
        }
        this.id = id;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void addReading(Reading reading) {
        this.run.addReading(this, reading);
    }

    public String getType() {
        return type.toString();
    }
}
