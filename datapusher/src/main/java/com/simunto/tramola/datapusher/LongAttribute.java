package com.simunto.tramola.datapusher;

/**
 * @author mrieser
 */
class LongAttribute extends Attribute {

	final long value;

	LongAttribute(String name, long value) {
		super(name);
		this.value = value;
	}
}
