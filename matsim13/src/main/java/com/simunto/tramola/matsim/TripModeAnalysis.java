package com.simunto.tramola.matsim;

import com.simunto.tramola.datapusher.Reading;
import com.simunto.tramola.datapusher.Run;
import com.simunto.tramola.datapusher.Serie;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.router.MainModeIdentifier;
import org.matsim.core.router.TripStructureUtils;
import org.matsim.core.router.TripStructureUtils.Trip;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author mrieser / Simunto GmbH
 */
public class TripModeAnalysis implements IterationEndsListener {

	private final Scenario scenario;
	private final MainModeIdentifier matsimMainModeIdentifier;
	private final MainModeIdentifier tramolaMainModeIdentifier = new TramolaMainModeIdentifier();
	private final Serie modeStats;

	public TripModeAnalysis(Scenario scenario, Run run, MainModeIdentifier mainModeIdentifier) {
		this.scenario = scenario;
		this.modeStats = run.addSerie("modeStats", Serie.SerieType.LONG);
		this.matsimMainModeIdentifier = mainModeIdentifier;
	}

	@Override
	public void notifyIterationEnds(IterationEndsEvent event) {
		Population population = this.scenario.getPopulation();
		Map<String, Integer> modeCnt = new TreeMap<>();

		for (Person person : population.getPersons().values()) {
			Plan plan = person.getSelectedPlan() ;
			List<Trip> trips = TripStructureUtils.getTrips(plan);
			for (Trip trip : trips) {
				String mode = this.findMode(trip);
				modeCnt.compute(mode, (key, old) -> old == null ? 1 : old + 1);
			}
		}

		Reading.ReadingBuilder reading = Reading.build().iteration(event.getIteration());
		for (Map.Entry<String, Integer> e : modeCnt.entrySet()) {
			reading.value(e.getKey(), e.getValue());
		}
		this.modeStats.addReading(reading.get());
	}

	private String findMode(Trip trip) {
		String mainMode = "unknown";
		try {
			mainMode = this.matsimMainModeIdentifier.identifyMainMode(trip.getTripElements());
		} catch (Exception e1) {
			try {
				mainMode = this.tramolaMainModeIdentifier.identifyMainMode(trip.getTripElements());
			} catch (Exception e2) {
				// ignore
			}
		}
		return mainMode;
	}

}
