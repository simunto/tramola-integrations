package com.simunto.tramola.changeset;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterators;
import org.matsim.api.core.v01.Scenario;
import org.matsim.core.utils.geometry.CoordinateTransformation;
import org.matsim.core.utils.geometry.transformations.IdentityTransformation;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * A Changeset is a collection of one or more changes ("modifications") to be applied to the files contained in
 * a dataset (e.g. to the network- or transitschedule-files).
 */
public class Changeset implements Iterable<Change> {

    private static final ObjectMapper mapper = new ObjectMapper();

    private String name;

    @JsonProperty
    Change[] modifications;

    /**
     * Read and return changeset.
     */
    public static Changeset read(Reader reader) throws IOException {
        return mapper.readValue(reader, Changeset.class);
    }

    /**
     * Write a changeset.
     */
    public static void write(Writer writer, Changeset cs) throws IOException {
        mapper.writeValue(writer, cs);
    }

    /**
     * Apply changeset using {@link ChangesetApplier}.
     */
    public static void apply(Scenario scenario, CoordinateTransformation ct, Changeset... changesets) {
        ChangesetApplier applier = new ChangesetApplier(scenario, ct);

        applier.applyChanges(Arrays.asList(changesets));
    }

    /**
     * Apply changeset using {@link ChangesetApplier}, without coordinate transformation.
     */
    public static void apply(Scenario scenario, Changeset... changesets) {
        apply(scenario, new IdentityTransformation(), changesets);
    }

    public Changeset() {
        this.modifications = new Change[0];
    }

    public Changeset(String name, Change... modifications) {
        this.name = name;
        this.modifications = modifications;
    }

    public String getName() {
        return name;
    }

    @Override
    public Iterator<Change> iterator() {
        return Iterators.forArray(modifications);
    }

    @Override
    public Spliterator<Change> spliterator() {
        return Arrays.spliterator(modifications);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Changeset changes = (Changeset) o;
        return Objects.equals(name, changes.name) && Arrays.equals(modifications, changes.modifications);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name);
        result = 31 * result + Arrays.hashCode(modifications);
        return result;
    }

    @Override
    public String toString() {
        Map<String, Long> perType = Arrays.stream(modifications)
                .collect(Collectors.groupingBy(m -> m.type, Collectors.counting()));

        return "Changeset{" +
                "name='" + name + '\'' +
                ", modifications=" + perType +
                '}';
    }
}
