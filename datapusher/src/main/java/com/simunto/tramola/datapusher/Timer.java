package com.simunto.tramola.datapusher;

/**
 * @author mrieser
 */
public class Timer {

	private static TimeProvider INSTANCE = System::currentTimeMillis;

	/**
	 * Sets a custom TimeProvider. Should only be used for tests.
	 *
	 * @param timer the time provider to use, if <code>null</code> then the default System time will be used.
	 */
	public static void setTimeProvider(TimeProvider timer) {
		if (timer == null) {
			INSTANCE = System::currentTimeMillis;
		} else {
			INSTANCE = timer;
		}
	}

	public static long currentTimeMillis() {
		return Timer.INSTANCE.currentTimeMillis();
	}

	@FunctionalInterface
	public interface TimeProvider {
		long currentTimeMillis();
	}

}
