package com.simunto.tramola.matsim;

import com.simunto.tramola.datapusher.Reading;
import com.simunto.tramola.datapusher.Run;
import com.simunto.tramola.datapusher.Serie;
import com.simunto.tramola.datapusher.Timer;
import com.simunto.tramola.datapusher.TramolaClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.matsim.analysis.ScoreStatsControlerListener;
import org.matsim.core.config.ConfigWriter;
import org.matsim.core.controler.events.AfterMobsimEvent;
import org.matsim.core.controler.events.BeforeMobsimEvent;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.events.IterationStartsEvent;
import org.matsim.core.controler.events.ShutdownEvent;
import org.matsim.core.controler.events.StartupEvent;
import org.matsim.core.controler.listener.AfterMobsimListener;
import org.matsim.core.controler.listener.BeforeMobsimListener;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.controler.listener.IterationStartsListener;
import org.matsim.core.controler.listener.ShutdownListener;
import org.matsim.core.controler.listener.StartupListener;
import org.matsim.core.router.AnalysisMainModeIdentifier;

import jakarta.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author mrieser
 */
public class TramolaControlerListener implements StartupListener, IterationStartsListener, IterationEndsListener, BeforeMobsimListener, AfterMobsimListener, ShutdownListener {

	private static final Logger log = LogManager.getLogger(TramolaControlerListener.class);

	private final TramolaClient client;
	private final ScoreStatsControlerListener scoreStatsListener;
	private final TramolaIntegrationConfigGroup tramolaConfig;
	private final AnalysisMainModeIdentifier mainModeIdentifier;
	private Run run;
	private Serie stopWatch;
	private Serie scoreStats;
	private Serie stuckStats;
	private final StuckAgentsCollector stuckAgents = new StuckAgentsCollector();
	private TripModeAnalysis tripModes = null;
	private boolean hasShutdown = false;
	private long lastIterationStart = -1;
	private long lastBeforeMobsim = -1;

	@Inject
	public TramolaControlerListener(TramolaClient client, ScoreStatsControlerListener scoreStatsListener, TramolaIntegrationConfigGroup tramolaConfig, AnalysisMainModeIdentifier mainModeIdentifier) {
		this.client = client;
		this.scoreStatsListener = scoreStatsListener;
		this.tramolaConfig = tramolaConfig;
		this.mainModeIdentifier = mainModeIdentifier;
	}

	@Override
	public void notifyStartup(StartupEvent startupEvent) {
		this.run = this.client.getRegisteredRun();

		// custom stop watch
		this.stopWatch = this.run.addSerie("stopwatch", Serie.SerieType.LONG);
		this.stopWatch.addReading(Reading.build().value("startup", Timer.currentTimeMillis()).get());

		// config
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PrintWriter writer = new PrintWriter(bos, false, StandardCharsets.UTF_8);
		try {
			new ConfigWriter(startupEvent.getServices().getConfig()).writeStream(writer);
			try {
				this.run.addFile("config.xml", bos.toByteArray());
			} catch (UncheckedIOException e) {
				if (this.tramolaConfig.isRequireConnection()) {
					throw e;
				}
				log.error("Could not submit file: config.xml", e);
			}
		} catch (Exception e) {
			log.error("Could not write config", e);
		}

		// score stats
		if (this.scoreStatsListener != null) {
			this.scoreStats = this.run.addSerie("scoreStats", Serie.SerieType.DOUBLE);
		}

		this.stuckStats = this.run.addSerie("stuckAgents", Serie.SerieType.LONG);
		startupEvent.getServices().getEvents().addHandler(this.stuckAgents);

		this.tripModes = new TripModeAnalysis(
				startupEvent.getServices().getScenario(),
				this.run,
				this.mainModeIdentifier);

		Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));

		ScenarioStats.submit(startupEvent.getServices().getScenario(), this.run);
	}

	@Override
	public void notifyIterationStarts(IterationStartsEvent iterationStartsEvent) {
		this.lastIterationStart = Timer.currentTimeMillis();
		this.stopWatch.addReading(Reading.build().iteration(iterationStartsEvent.getIteration()).value("iterationStarts", this.lastIterationStart).get());
	}

	@Override
	public void notifyIterationEnds(IterationEndsEvent iterationEndsEvent) {
		int iteration = iterationEndsEvent.getIteration();
		long iterationEnd = Timer.currentTimeMillis();
		this.stopWatch.addReading(Reading.build().iteration(iteration)
			.value("iterationEnds", iterationEnd)
			.value("iterationDuration", (iterationEnd - this.lastIterationStart) / 1000.0)
			.get());

		if (this.scoreStats != null) {
			Map<ScoreStatsControlerListener.ScoreItem, Map<Integer, Double>> scoresHistory = this.scoreStatsListener.getScoreHistory();
			this.scoreStats.addReading(
					Reading.build().iteration(iteration)
							.value("executed", scoresHistory.get(ScoreStatsControlerListener.ScoreItem.executed).get(iteration))
							.value("average", scoresHistory.get(ScoreStatsControlerListener.ScoreItem.average).get(iteration))
							.value("worst", scoresHistory.get(ScoreStatsControlerListener.ScoreItem.worst).get(iteration))
							.value("best", scoresHistory.get(ScoreStatsControlerListener.ScoreItem.best).get(iteration))
							.get());
		}

		this.stuckStats.addReading(Reading.build().iteration(iteration).value("stuck", this.stuckAgents.counter).get());
		this.tripModes.notifyIterationEnds(iterationEndsEvent);
	}

	@Override
	public void notifyBeforeMobsim(BeforeMobsimEvent event) {
		this.lastBeforeMobsim = Timer.currentTimeMillis();
		this.stopWatch.addReading(Reading.build().iteration(event.getIteration())
			.value("replanningDuration", (this.lastBeforeMobsim - this.lastIterationStart) / 1000.0)
			.get());
	}

	@Override
	public void notifyAfterMobsim(AfterMobsimEvent event) {
		long now = Timer.currentTimeMillis();
		this.stopWatch.addReading(Reading.build().iteration(event.getIteration())
			.value("mobsimDuration", (now - this.lastBeforeMobsim) / 1000.0)
			.get());
	}

	@Override
	public void notifyShutdown(ShutdownEvent shutdownEvent) {
		String outputDirectory = shutdownEvent.getServices().getControlerIO().getOutputPath();
		File outputDir = new File(outputDirectory);

		Pattern resultFilesPattern = Pattern.compile(this.tramolaConfig.getResultFiles());
		Matcher matcher = resultFilesPattern.matcher("");
		this.run.uploadFilesInDirectory(matcher, outputDir);
		Run.createOutputFile(outputDir);


		this.shutdown();
	}

	public void shutdown() {
		if (!this.hasShutdown) {
			this.stopWatch.addReading(Reading.build().value("shutdown", Timer.currentTimeMillis()).get());
			this.run.addEvent("runFinished", "");
			this.run.close();
			this.hasShutdown = true;
		}
	}
}
