package com.simunto.tramola.matsim;

import ch.sbb.matsim.routing.pt.raptor.SwissRailRaptorModule;
import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Scenario;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.scenario.ScenarioUtils;

/**
 * @author mrieser / Simunto
 */
public class Run {

	private static final Logger log = Logger.getLogger(Run.class);

	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("No config file specified.");
			return;
		}

		System.setProperty("matsim.preferLocalDtds", "true");

		int statusCode = 0;
		try {

			String configFilename = args[0];
			Config config = ConfigUtils.loadConfig(configFilename);

			TramolaMatsimIntegration tramola = new TramolaMatsimIntegration(config);
			tramola.init();

			config.controler().setOverwriteFileSetting(OutputDirectoryHierarchy.OverwriteFileSetting.overwriteExistingFiles);

			Scenario scenario = ScenarioUtils.loadScenario(config);
			Controler controler = new Controler(scenario);

			if (config.transit().isUseTransit()) {
				controler.addOverridingModule(new SwissRailRaptorModule());
			}

			controler.addOverridingModule(tramola);

			controler.run();

		} catch (Exception e) {
			e.printStackTrace();
			statusCode = 1;
		} finally {
			System.exit(statusCode); // I've observed cases where daemon threads kept the JVM running, although they shouldn't, so force the exit
		}
	}
}
