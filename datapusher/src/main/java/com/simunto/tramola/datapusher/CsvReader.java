package com.simunto.tramola.datapusher;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * @author mrieser / Simunto
 */
class CsvReader implements Closeable {

	private char fieldSeparator = ',';
	private final char quoteCharacter = '"';

	private final Reader reader;
	private final char[] buf = new char[1024];
	private int buflength = 0;
	private int bufpos = 0;
	private boolean lastWasReturn = false;
	private boolean headerRead = false;

	public CsvReader(final String filename) throws IOException {
		this.reader = getBufferedReader(filename, StandardCharsets.UTF_8);
	}

	public CsvReader(final String filename, final Charset charset) throws IOException {
		this.reader = getBufferedReader(filename, charset);
	}

	public CsvReader(final InputStream stream) {
		this.reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
	}

	public CsvReader(final Reader reader) {
		this.reader = new BufferedReader(reader);
	}

	public void setFieldSeparator(final char fieldSeparator) {
		this.fieldSeparator = fieldSeparator;
	}

	public String[] readHeader() throws IOException {
		if (this.headerRead) {
			throw new IllegalStateException("CSV header already read.");
		}
		return this.readRecord();
	}

	public Map<String, Integer> buildIndexLookupFromHeader(final String[] header) {
		Map<String, Integer> lookup = new LinkedHashMap<>(header.length);
		for (int i=0; i<header.length; i++) {
			lookup.put(header[i],i);
		}
		return lookup;
	}

	/**
	 * @return <code>null</code> if no more records are available.
	 * @throws IOException
	 */
	public String[] readRecord() throws IOException {
		this.headerRead = true;
		ArrayList<String> parts = new ArrayList<>();
		boolean recordComplete = false;
		boolean quoteOpen = false;
		StringBuilder fieldValue = new StringBuilder(32);
		boolean useNextQuote = false;
		boolean hasFields = false;
		while (!recordComplete) {
			if (this.bufpos == this.buflength) {
				updateBuffer();
			}
			if (this.buflength < 0) {
				if (hasFields) {
					parts.add(fieldValue.toString());
					recordComplete = true;
					break;
				} else {
					this.reader.close(); // never trust the user, close it ourselves
					return null;
				}
			}
			while (this.bufpos < this.buflength) {
				char c = this.buf[this.bufpos];
				this.bufpos++;
				if (c == '\r' && !quoteOpen) {
					parts.add(fieldValue.toString());
					fieldValue.setLength(0);
					this.lastWasReturn = true;
					recordComplete = true;
					break;
				} else if (c == '\n' && !quoteOpen) {
					if (this.lastWasReturn) {
						this.lastWasReturn = false;
						// ignore this character, it's part of \r\n
					} else {
						parts.add(fieldValue.toString());
						fieldValue.setLength(0);
						recordComplete = true;
						break;
					}
				} else if (c == this.fieldSeparator && !quoteOpen) {
					parts.add(fieldValue.toString());
					fieldValue.setLength(0);
					this.lastWasReturn = false;
					hasFields = true;
					useNextQuote = false;
				} else if (c == this.quoteCharacter) {
					if (fieldValue.length() == 0 && !quoteOpen && !useNextQuote) {
						quoteOpen = true;
						useNextQuote = false;
					} else {
						quoteOpen = !quoteOpen;
						if (quoteOpen) {
							fieldValue.append(c);
						}
						useNextQuote = !useNextQuote;
					}
					this.lastWasReturn = false;
					hasFields = true;
				} else {
					fieldValue.append(c);
					this.lastWasReturn = false;
					hasFields = true;
				}
			}
		}

		return parts.toArray(new String[0]);
	}

	private void updateBuffer() throws IOException {
		this.buflength = this.reader.read(this.buf);
		this.bufpos = 0;
	}

	@Override
	public void close() throws IOException {
		this.reader.close();
	}


	private static final String GZ = ".gz";
	private static final int BUFFER_SIZE = 4*1024*1024;
	private static final int GZIP_BUFFER_SIZE = 8*1024;

	public static BufferedReader getBufferedReader(final String filename, final Charset charset) throws IOException {
		BufferedReader infile = null;
		if (filename == null) {
			throw new FileNotFoundException("No filename given (filename == null)");
		}
		String lcFilename = filename.toLowerCase(Locale.ROOT);
		if (new File(filename).exists()) {
			FileChannel channel = new RandomAccessFile(filename, "r").getChannel();
			InputStream fis = Channels.newInputStream(channel);
			if (lcFilename.endsWith(GZ)) {
				infile = new BufferedReader(new InputStreamReader(new UnicodeInputStream(new GZIPInputStream(fis, GZIP_BUFFER_SIZE)), charset), BUFFER_SIZE);
			} else {
				infile = new BufferedReader(new InputStreamReader(new UnicodeInputStream(fis), charset), BUFFER_SIZE);
			}
		} else if (new File(filename + GZ).exists()) {
			FileChannel channel = new RandomAccessFile(filename + GZ, "r").getChannel();
			InputStream fis = Channels.newInputStream(channel);
			infile = new BufferedReader(new InputStreamReader(new UnicodeInputStream(new GZIPInputStream(fis, GZIP_BUFFER_SIZE)), charset), BUFFER_SIZE);
		}
		if (infile == null) {
			throw new FileNotFoundException(filename);
		}
		return infile;
	}

}
