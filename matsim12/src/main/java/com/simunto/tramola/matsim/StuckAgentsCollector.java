package com.simunto.tramola.matsim;

import org.matsim.api.core.v01.events.PersonStuckEvent;
import org.matsim.api.core.v01.events.handler.PersonStuckEventHandler;

/**
 * @author mrieser / Simunto GmbH
 */
public class StuckAgentsCollector implements PersonStuckEventHandler {

	int counter = 0;

	@Override
	public void handleEvent(PersonStuckEvent personStuckEvent) {
		this.counter++;
	}

	@Override
	public void reset(int iteration) {
		this.counter = 0;
	}

}
