package com.simunto.tramola.datapusher;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.javalin.Javalin;
import io.javalin.http.Context;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mrieser
 */
public class TramolaClientTest {

	@Test
	public void testAPI() {
		List<Call> calls = new ArrayList<>();
		int testPort = 50000 + (int) (Math.random() * 1000);

		Javalin http = Javalin.create();
		try {
			http.before(c -> calls.add(new Call(c)));
			ObjectMapper objectMapper = new ObjectMapper();
			http.post("/api/v1/datasets/datasets/*/tables", c -> {
				c.res.setContentType("application/json");
				String name = objectMapper.readTree(c.body()).get("name").asText();
				c.result("{\"id\":1, \"name\": \""+name+"\"}");
			});
			http.post("*", c -> {
				c.res.setContentType("application/json");
				c.result("{\"id\":1}");
			});
			http.start(testPort);

			// ***************

			TramolaClient mc = new TramolaClient("http://localhost:" + testPort, "apikeyABC", "apipassword123");

			Timer.setTimeProvider(() -> 1_000_000_000L);

			Run run1 = mc.registerRun(Run.build()
					.withName("My First Run")
					.withAttribute("a", "abc")
					.withAttribute("b", 27)
					.withAttribute("c", 23.11)
					.get()
			);

			Serie serie1 = run1.addSerie("modalsplit", Serie.SerieType.DOUBLE);

			Timer.setTimeProvider(() -> 1_000_000_005L);

			serie1.addReading(Reading.build()
					.iteration(10)
					.value("car", 0.1)
					.value("pt", 0.8)
					.value("bike", 0.1)
					.get()
			);

			Timer.setTimeProvider(() -> 1_000_000_0010L);
			run1.addEvent("finished", "now");

			run1.close();

			// **************

			String apikey = "apikeyABC";

			Assert.assertEquals(4, calls.size());

			Call call1 = calls.get(0);
			Assert.assertEquals("POST", call1.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call1.contentType);
			Assert.assertEquals("{\"type\":\"run\",\"name\":\"My First Run\",\"timestamp\":1000000000,\"attributes\":{\"a\":\"abc\",\"b\":27,\"c\":23.11}}", call1.body);
			Assert.assertEquals("tramola-apikey " + apikey, call1.authHeader);

			Call call2 = calls.get(1);
			Assert.assertEquals("POST", call2.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call2.contentType);
			Assert.assertEquals("{\"name\":\"modalsplit\",\"fields\":[{\"name\":\"key\",\"type\":\"STRING\"},{\"name\":\"time\",\"type\":\"LONG\"},{\"name\":\"value\",\"type\":\"DOUBLE\"},{\"name\":\"iteration\",\"type\":\"LONG\"}]}", call2.body);
			Assert.assertEquals("tramola-apikey " + apikey, call2.authHeader);

			Call call3 = calls.get(2);
			Assert.assertEquals("PATCH", call3.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call3.contentType);
			Assert.assertEquals("/api/v1/datasets/datasets/1/tables/modalsplit", call3.pathInfo);
			Assert.assertEquals("[{\"time\":1000000005,\"iteration\":10,\"key\":\"car\",\"value\":0.1},{\"time\":1000000005,\"iteration\":10,\"key\":\"pt\",\"value\":0.8},{\"time\":1000000005,\"iteration\":10,\"key\":\"bike\",\"value\":0.1}]", call3.body);
			Assert.assertEquals("tramola-apikey " + apikey, call3.authHeader);

			Call call4 = calls.get(3);
			Assert.assertEquals("POST", call4.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call4.contentType);
			Assert.assertEquals("{\"timestamp\":10000000010,\"type\":\"finished\",\"data\":\"now\"}", call4.body);
			Assert.assertEquals("tramola-apikey " + apikey, call4.authHeader);

		} finally {
			http.stop();
		}
	}

	@Test
	public void test_regularPushing() throws Exception {
		List<Call> calls = new ArrayList<>();
		int testPort = 50000 + (int) (Math.random() * 1000);

		Javalin http = Javalin.create();

		try {
			http.before(c -> calls.add(new Call(c)));
			ObjectMapper objectMapper = new ObjectMapper();
			http.post("/api/v1/datasets/datasets/*/tables", c -> {
				c.res.setContentType("application/json");
				String name = objectMapper.readTree(c.body()).get("name").asText();
				c.result("{\"id\":1, \"name\": \""+name+"\"}");
			});
			http.post("*", c -> {
				c.res.setContentType("application/json");
				c.result("{\"id\":1}");
			});
			http.start(testPort);

			// ***************

			TramolaClient mc = new TramolaClient("http://localhost:" + testPort, "apikeyABC", "apipassword123");
			String apikey = "apikeyABC";

//			Timer.setTimeProvider(() -> 1_000_000_000L);

			Timer.setTimeProvider(() -> 1_000_000_000L);

			Run run1 = mc.registerRun(Run.build()
					.withName("My First Run")
					.withAttribute("a", "abc")
//					.withAttribute("b", 27)
//					.withAttribute("c", 23.11)
					.get()
			);

			Serie serie1 = run1.addSerie("modalsplit", Serie.SerieType.DOUBLE);

			Assert.assertEquals(2, calls.size());

			Call call1 = calls.get(0);
			Assert.assertEquals("POST", call1.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call1.contentType);
			Assert.assertEquals("{\"type\":\"run\",\"name\":\"My First Run\",\"timestamp\":1000000000,\"attributes\":{\"a\":\"abc\"}}", call1.body);
			Assert.assertEquals("tramola-apikey " + apikey, call1.authHeader);

			Call call2 = calls.get(1);
			Assert.assertEquals("POST", call2.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call2.contentType);
			Assert.assertEquals("{\"name\":\"modalsplit\",\"fields\":[{\"name\":\"key\",\"type\":\"STRING\"},{\"name\":\"time\",\"type\":\"LONG\"},{\"name\":\"value\",\"type\":\"DOUBLE\"},{\"name\":\"iteration\",\"type\":\"LONG\"}]}", call2.body);
			Assert.assertEquals("tramola-apikey " + apikey, call2.authHeader);

			Timer.setTimeProvider(() -> 1_000_000_005L);

			serie1.addReading(Reading.build()
					.iteration(10)
					.value("car", 0.1)
					.value("pt", 0.8)
					.value("bike", 0.1)
					.get()
			);

			Assert.assertEquals("the reading should not yet have been submitted", 2, calls.size());

			Thread.sleep(7000);

			Assert.assertEquals("the reading should have been submitted now.", 3, calls.size());

			Call call3 = calls.get(2);
			Assert.assertEquals("PATCH", call3.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call3.contentType);
			Assert.assertEquals("[{\"time\":1000000005,\"iteration\":10,\"key\":\"car\",\"value\":0.1},{\"time\":1000000005,\"iteration\":10,\"key\":\"pt\",\"value\":0.8},{\"time\":1000000005,\"iteration\":10,\"key\":\"bike\",\"value\":0.1}]", call3.body);
			Assert.assertEquals("tramola-apikey " + apikey, call3.authHeader);

			Timer.setTimeProvider(() -> 1_000_000_010L);

			serie1.addReading(Reading.build()
					.iteration(20)
					.value("car", 0.2)
					.value("pt", 0.5)
					.value("bike", 0.3)
					.get()
			);

			Assert.assertEquals("the reading should not yet have been submitted", 3, calls.size());

			Thread.sleep(5000);

			Assert.assertEquals("the reading should have been submitted now.", 4, calls.size());

			Call call4 = calls.get(3);
			Assert.assertEquals("PATCH", call4.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call4.contentType);
			Assert.assertEquals("[{\"time\":1000000010,\"iteration\":20,\"key\":\"car\",\"value\":0.2},{\"time\":1000000010,\"iteration\":20,\"key\":\"pt\",\"value\":0.5},{\"time\":1000000010,\"iteration\":20,\"key\":\"bike\",\"value\":0.3}]", call4.body);
			Assert.assertEquals("tramola-apikey " + apikey, call4.authHeader);

			Timer.setTimeProvider(() -> 1_000_000_0015L);
			run1.addEvent("finished", "now");

			run1.close();

			// **************

			Assert.assertEquals(5, calls.size());

			Call call5 = calls.get(4);
			Assert.assertEquals("POST", call5.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call5.contentType);
			Assert.assertEquals("{\"timestamp\":10000000015,\"type\":\"finished\",\"data\":\"now\"}", call5.body);
			Assert.assertEquals("tramola-apikey " + apikey, call5.authHeader);

		} finally {
			http.stop();
		}
	}

	@Test
	public void test_regularPushing_withEvents() throws Exception {
		List<Call> calls = new ArrayList<>();
		int testPort = 50000 + (int) (Math.random() * 1000);

		Javalin http = Javalin.create();
		try {
			http.before(c -> calls.add(new Call(c)));
			ObjectMapper objectMapper = new ObjectMapper();
			http.post("/api/v1/datasets/datasets/*/tables", c -> {
				c.res.setContentType("application/json");
				String name = objectMapper.readTree(c.body()).get("name").asText();
				c.result("{\"id\":1, \"name\": \""+name+"\"}");
			});
			http.post("*", c -> {
				c.res.setContentType("application/json");
				c.result("{\"id\":1}");
			});
			http.start(testPort);

			// ***************

			TramolaClient mc = new TramolaClient("http://localhost:" + testPort, "apikeyABC", "apipassword123");
			String apikey = "apikeyABC";

			Timer.setTimeProvider(() -> 1_000_000_000L);

			Run run1 = mc.registerRun(Run.build()
					.withName("My First Run")
					.withAttribute("a", "abc")
					.get()
			);

			Serie serie1 = run1.addSerie("modalsplit", Serie.SerieType.DOUBLE);
			Serie serie2 = run1.addSerie("memory", Serie.SerieType.DOUBLE);

			Assert.assertEquals(3, calls.size());

			Call call1 = calls.get(0);
			Assert.assertEquals("POST", call1.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call1.contentType);
			Assert.assertEquals("{\"type\":\"run\",\"name\":\"My First Run\",\"timestamp\":1000000000,\"attributes\":{\"a\":\"abc\"}}", call1.body);
			Assert.assertEquals("tramola-apikey " + apikey, call1.authHeader);

			Call call2 = calls.get(1);
			Assert.assertEquals("POST", call2.requestMethod);
			Assert.assertEquals("/api/v1/datasets/datasets/1/tables", call2.pathInfo);
			Assert.assertEquals("application/json; charset=UTF-8", call2.contentType);
			Assert.assertEquals("{\"name\":\"modalsplit\",\"fields\":[{\"name\":\"key\",\"type\":\"STRING\"},{\"name\":\"time\",\"type\":\"LONG\"},{\"name\":\"value\",\"type\":\"DOUBLE\"},{\"name\":\"iteration\",\"type\":\"LONG\"}]}", call2.body);
			Assert.assertEquals("tramola-apikey " + apikey, call2.authHeader);

			Call call3 = calls.get(2);
			Assert.assertEquals("/api/v1/datasets/datasets/1/tables", call3.pathInfo);

			Timer.setTimeProvider(() -> 1_000_000_005L);

			run1.addEvent("startup", "");

			Thread.sleep(1000);

			Assert.assertEquals(4, calls.size());

			Call call4 = calls.get(3);
			Assert.assertEquals("POST", call4.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call4.contentType);
			Assert.assertEquals("{\"timestamp\":1000000005,\"type\":\"startup\",\"data\":\"\"}", call4.body);
			Assert.assertEquals("tramola-apikey " + apikey, call4.authHeader);

			Timer.setTimeProvider(() -> 1_000_000_010L);
			serie1.addReading(Reading.build()
					.iteration(10)
					.value("car", 0.1)
					.value("pt", 0.8)
					.value("bike", 0.1)
					.get()
			);

			serie2.addReading(Reading.build()
					.iteration(10)
					.value("used", 239092)
					.value("free", 905904)
					.value("total", 1134990)
					.get()
			);

			Assert.assertEquals("the reading should not yet have been submitted", 4, calls.size());

			Thread.sleep(7000);

			Assert.assertEquals("the reading should have been submitted now.", 6, calls.size());

			Call call5 = calls.get(4);
			Assert.assertEquals("PATCH", call5.requestMethod);
			Assert.assertEquals("/api/v1/datasets/datasets/1/tables/memory", call5.pathInfo);


			Call call6 = calls.get(5);
			Assert.assertEquals("PATCH", call6.requestMethod);
			Assert.assertEquals("/api/v1/datasets/datasets/1/tables/modalsplit", call6.pathInfo);

			Timer.setTimeProvider(() -> 1_000_000_0015L);
			run1.addEvent("finished", "now");

			run1.close();

			// **************

			Assert.assertEquals(7, calls.size());

			Call call7 = calls.get(6);
			Assert.assertEquals("POST", call7.requestMethod);
			Assert.assertEquals("application/json; charset=UTF-8", call7.contentType);
			Assert.assertEquals("{\"timestamp\":10000000015,\"type\":\"finished\",\"data\":\"now\"}", call7.body);
			Assert.assertEquals("tramola-apikey " + apikey, call7.authHeader);

		} finally {
			http.stop();
		}
	}

	private static class Call {
		final String requestMethod;
		final String pathInfo;
		final String body;
		final String contentType;
		final String authHeader;

		Call(Context c) {
			System.out.println("Call received: " + c.url() + " >>> " + c.body());
			this.requestMethod = c.req.getMethod();
			this.pathInfo = c.req.getPathInfo();
			this.body = c.body();
			this.contentType = c.req.getContentType();
			this.authHeader = c.header("Authorization");
		}
	}

}
