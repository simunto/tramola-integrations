package com.simunto.tramola.matsim;

import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.router.AnalysisMainModeIdentifier;
import org.matsim.core.router.MainModeIdentifier;

import java.util.ArrayList;
import java.util.List;

/**
 * A {@link MainModeIdentifier} that tries to figure out the main mode from a transport engineering perspective.
 */
public final class TramolaMainModeIdentifier implements AnalysisMainModeIdentifier {

	private final List<String> secondaryModeHierarchy = new ArrayList<>();
	private final List<String> primaryModeHierarchy = new ArrayList<>();

	public TramolaMainModeIdentifier() {
		primaryModeHierarchy.add(TransportMode.car);
		primaryModeHierarchy.add(TransportMode.ride);
		primaryModeHierarchy.add(TransportMode.pt);
		primaryModeHierarchy.add(TransportMode.drt);
		primaryModeHierarchy.add(TransportMode.bike);

		secondaryModeHierarchy.add(TransportMode.walk);
		secondaryModeHierarchy.add(TransportMode.transit_walk);
		secondaryModeHierarchy.add(TransportMode.other);
		secondaryModeHierarchy.add("undefined");
		secondaryModeHierarchy.add(TransportMode.non_network_walk);
	}

	@Override
	public String identifyMainMode(List<? extends PlanElement> planElements) {
		int primaryModeIndex = this.primaryModeHierarchy.size();
		int secondaryModeIndex = this.secondaryModeHierarchy.size();
		String customMode = null;

		for (PlanElement pe : planElements) {
			if (pe instanceof Leg) {
				String mode = ((Leg) pe).getMode();
				int index = this.primaryModeHierarchy.indexOf(mode);
				if (index >= 0 && index < primaryModeIndex) {
					primaryModeIndex = index;
				}
				if (index < 0) {
					index = this.secondaryModeHierarchy.indexOf(mode);
					if (index >= 0 && index < secondaryModeIndex) {
						secondaryModeIndex = index;
					} else if (customMode == null) {
						customMode = mode;
					}
				}
			}
		}

		if (primaryModeIndex < this.primaryModeHierarchy.size()) {
			return this.primaryModeHierarchy.get(primaryModeIndex);
		}
		if (customMode != null) {
			return customMode;
		}
		if (secondaryModeIndex < this.secondaryModeHierarchy.size()) {
			return this.secondaryModeHierarchy.get(secondaryModeIndex);
		}
		return "unknown";
	}
}
