package com.simunto.tramola.matsim;

import org.matsim.core.config.ReflectiveConfigGroup;

/**
 * @author mrieser / Simunto
 */
public class TramolaIntegrationConfigGroup extends ReflectiveConfigGroup {

	public static final String GROUP_NAME = "tramola";

	private static final String PARAM_URL = "url";
	private static final String PARAM_APIKEY = "apiKey";
	private static final String PARAM_APIPW = "apiPassword";
	private static final String PARAM_GROUP = "group";
	private static final String PARAM_DEBUG = "debug";
	private static final String PARAM_ENABLED = "enabled";
	private static final String PARAM_REQUIRECONNECTION = "requireConnection";
	private static final String PARAM_RESULTFILES = "resultFiles";
	private static final String PARAM_DATASET_ID = "datasetId";

	private String url;
	private String apiKey;
	private String apiPassword;
	private String group = null;
	private boolean debug = false;
	private boolean enabled = true;
	private boolean requireConnection = false;
	private String resultFiles = ".*output.*\\.xml.*|.*output.*\\.csv.*|modules.dot|logfile.*\\.log";
	private long datasetId = -1;

	public TramolaIntegrationConfigGroup() {
		super(GROUP_NAME);
	}

	@StringSetter(PARAM_URL)
	public void setUrl(String url) {
		this.url = url;
	}

	@StringGetter(PARAM_URL)
	public String getUrl() {
		return this.url;
	}

	@StringSetter(PARAM_APIKEY)
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	@StringGetter(PARAM_APIKEY)
	public String getApiKey() {
		return this.apiKey;
	}

	@StringSetter(PARAM_APIPW)
	public void setApiPassword(String apiPassword) {
		this.apiPassword = apiPassword;
	}

	@StringGetter(PARAM_APIPW)
	public String getApiPassword() {
		return this.apiPassword;
	}

	@StringSetter(PARAM_GROUP)
	public void setGroup(String group) {
		this.group = group;
	}

	@StringGetter(PARAM_GROUP)
	public String getGroup() {
		return this.group;
	}

	@StringSetter(PARAM_DEBUG)
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	@StringGetter(PARAM_DEBUG)
	public boolean isDebug() {
		return this.debug;
	}

	@StringSetter(PARAM_ENABLED)
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@StringGetter(PARAM_ENABLED)
	public boolean isEnabled() {
		return this.enabled;
	}

	@StringSetter(PARAM_REQUIRECONNECTION)
	public void setRequireConnection(boolean requireConnection) {
		this.requireConnection = requireConnection;
	}

	@StringGetter(PARAM_REQUIRECONNECTION)
	public boolean isRequireConnection() {
		return this.requireConnection;
	}

	@StringSetter(PARAM_RESULTFILES)
	public void setResultFiles(String resultFiles) {
		this.resultFiles = resultFiles;
	}

	@StringGetter(PARAM_RESULTFILES)
	public String getResultFiles() {
		return this.resultFiles;
	}

	@StringSetter(PARAM_DATASET_ID)
	public void setDatasetId(long datasetId) {
		this.datasetId = datasetId;
	}

	@StringGetter(PARAM_DATASET_ID)
	public long getDatasetId() {
		return datasetId;
	}
}
