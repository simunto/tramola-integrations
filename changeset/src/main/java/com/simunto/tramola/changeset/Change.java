package com.simunto.tramola.changeset;

import com.fasterxml.jackson.annotation.*;

import java.util.Map;
import java.util.Objects;

/**
 * Describes a single change to a MATSim data container (e.g. network, transit-schedule).
 */
public class Change {

	@JsonProperty(index = 0)
	public final String type;
	public final String dataset;

	@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		include = JsonTypeInfo.As.EXTERNAL_PROPERTY,
		property = "type")
	@JsonSubTypes({
		/* network changes */
		@JsonSubTypes.Type(value = AddNodePayload.class, name = AddNodePayload.TYPE),
		@JsonSubTypes.Type(value = UpdateNodePayload.class, name = UpdateNodePayload.TYPE),
		@JsonSubTypes.Type(value = DeleteNodePayload.class, name = DeleteNodePayload.TYPE),
		@JsonSubTypes.Type(value = NetworkGroupPayload.class, name = NetworkGroupPayload.TYPE),
		@JsonSubTypes.Type(value = AddLinkPayload.class, name = AddLinkPayload.TYPE),
		@JsonSubTypes.Type(value = UpdateLinkPayload.class, name = UpdateLinkPayload.TYPE),
		@JsonSubTypes.Type(value = DeleteLinkPayload.class, name = DeleteLinkPayload.TYPE),

		/* transit changes */
		@JsonSubTypes.Type(value = TransitLineGroupPayload.class, name = TransitLineGroupPayload.TYPE),
		@JsonSubTypes.Type(value = AddTransitStopPayload.class, name = AddTransitStopPayload.TYPE),
		@JsonSubTypes.Type(value = UpdateTransitStopPayload.class, name = UpdateTransitStopPayload.TYPE),
		@JsonSubTypes.Type(value = DeleteTransitStopPayload.class, name = DeleteTransitStopPayload.TYPE),
		@JsonSubTypes.Type(value = TransitStopGroupPayload.class, name = TransitStopGroupPayload.TYPE),
		@JsonSubTypes.Type(value = AddTransitLinePayload.class, name = AddTransitLinePayload.TYPE),
		@JsonSubTypes.Type(value = UpdateTransitLinePayload.class, name = UpdateTransitLinePayload.TYPE),
		@JsonSubTypes.Type(value = DeleteTransitLinePayload.class, name = DeleteTransitLinePayload.TYPE),
		@JsonSubTypes.Type(value = AddTransitRoutePayload.class, name = AddTransitRoutePayload.TYPE),
		@JsonSubTypes.Type(value = UpdateTransitRoutePayload.class, name = UpdateTransitRoutePayload.TYPE),
		@JsonSubTypes.Type(value = DeleteTransitRoutePayload.class, name = DeleteTransitRoutePayload.TYPE),
		@JsonSubTypes.Type(value = AddTransitRouteStopPayload.class, name = AddTransitRouteStopPayload.TYPE),
		@JsonSubTypes.Type(value = UpdateTransitRouteStopPayload.class, name = UpdateTransitRouteStopPayload.TYPE),
		@JsonSubTypes.Type(value = DeleteTransitRouteStopPayload.class, name = DeleteTransitRouteStopPayload.TYPE),
		@JsonSubTypes.Type(value = UpdateTransitRouteDeparturesPayload.class, name = UpdateTransitRouteDeparturesPayload.TYPE),
		@JsonSubTypes.Type(value = DeleteTransitNetworkRouteLinkPayload.class, name = DeleteTransitNetworkRouteLinkPayload.TYPE),
		@JsonSubTypes.Type(value = AddTransitNetworkRouteLinkPayload.class, name = AddTransitNetworkRouteLinkPayload.TYPE),
	})
	public ChangePayload payload;

	@JsonCreator
	Change(@JsonProperty("dataset") String dataset, @JsonProperty("type") String type, @JsonProperty("payload") ChangePayload payload) {
		this.dataset = dataset;
		this.type = type;
		this.payload = payload;

		if (!Objects.equals(type, payload.getType()))
			throw new IllegalArgumentException("Mismatching type and payload type");
	}

	/**
	 * Create a new change from payload.
	 */
	public Change(String dataset, ChangePayload payload) {
		this.dataset = dataset;
		this.type = payload.getType();
		this.payload = payload;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Change change = (Change) o;
		return Objects.equals(type, change.type) && Objects.equals(dataset, change.dataset) && Objects.equals(payload, change.payload);
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, dataset, payload);
	}

	/**
	 * Abstract class to describe a change.
	 */
	public abstract static class ChangePayload {

		@JsonIgnore
		public abstract String getType();

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			ChangePayload that = (ChangePayload) o;
			return Objects.equals(getType(), that.getType());
		}

		@Override
		public int hashCode() {
			return Objects.hash(getType());
		}
	}

	/**
	 * Create and add a new node to the network.
	 */
	public static class AddNodePayload extends ChangePayload {

		public static final String TYPE = "AddNode";

		public final String id;
		public final double lng;
		public final double lat;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public AddNodePayload(@JsonProperty("id") String id, @JsonProperty("lng") double lng, @JsonProperty("lat") double lat) {
			this.id = id;
			this.lng = lng;
			this.lat = lat;
		}


	}

	/**
	 * Change the location of a node.
	 */
	public static class UpdateNodePayload extends ChangePayload {

		public static final String TYPE = "UpdateNode";

		public final String id;
		public final double lng;
		public final double lat;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public UpdateNodePayload(@JsonProperty("id") String id, @JsonProperty("lng") double lng, @JsonProperty("lat") double lat) {
			this.id = id;
			this.lng = lng;
			this.lat = lat;
		}
	}

	/**
	 * Delete a node from the network.
	 */
	public static class DeleteNodePayload extends ChangePayload {

		public static final String TYPE = "DeleteNode";

		public final String id;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public DeleteNodePayload(@JsonProperty("id") String id) {
			this.id = id;
		}
	}

	/**
	 * A collection of multiple network-related changes.
	 */
	public static class NetworkGroupPayload extends ChangePayload {

		public static final String TYPE = "NetworkGroup";

		public final String groupType;
		public final Change[] modifications;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public NetworkGroupPayload(@JsonProperty("groupType") String groupType, @JsonProperty("modifications") Change... modifications) {
			this.groupType = groupType;
			this.modifications = modifications;
		}
	}

	/**
	 * Create and add a new link to the network with the specified properties.
	 */
	public static class AddLinkPayload extends ChangePayload {

		public static final String TYPE = "AddLink";

		public final String id;
		public final String fromId;
		public final String toId;
		public final Map<String, Object> properties;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public AddLinkPayload(@JsonProperty("id") String id, @JsonProperty("fromId") String fromId, @JsonProperty("toId") String toId,
													@JsonProperty("properties") Map<String, Object> properties) {
			this.id = id;
			this.fromId = fromId;
			this.toId = toId;
			this.properties = properties;
		}
	}

	/**
	 * Updates a link's properties.
	 */
	public static class UpdateLinkPayload extends ChangePayload {

		public static final String TYPE = "UpdateLink";

		public final String id;
		public final Map<String, Object> properties;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public UpdateLinkPayload(@JsonProperty("id") String id, @JsonProperty("properties") Map<String, Object> properties) {
			this.id = id;
			this.properties = properties;
		}
	}

	/**
	 * Delete a link from the network.
	 */
	public static class DeleteLinkPayload extends ChangePayload {

		public static final String TYPE = "DeleteLink";

		public final String id;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public DeleteLinkPayload(@JsonProperty("id") String id) {
			this.id = id;
		}
	}

	// === transit changes ===

	/**
	 * A collection of multiple, transit schedule-related changes.
	 */
	public static class TransitLineGroupPayload extends ChangePayload {

		public static final String TYPE = "TransitLineGroup";

		public final String groupType;
		public final Change[] modifications;

		@Override
		public String getType() {
			return TYPE;
		}


		@JsonCreator
		public TransitLineGroupPayload(@JsonProperty("groupType") String groupType, @JsonProperty("modifications") Change[] modifications) {
			this.groupType = groupType;
			this.modifications = modifications;
		}
	}


	/**
	 * Describes a transit stop with all its attributes.
	 */
	public static class PayloadTransitStop {
		public final double[] coordinates;
		public final boolean isBlocking;
		public final String linkId;
		public final String name;
		public final String stopAreaId;


		@JsonCreator
		public PayloadTransitStop(@JsonProperty("coordinates") double[] coordinates,
															@JsonProperty("isBlocking") boolean isBlocking,
															@JsonProperty("linkId") String linkId,
															@JsonProperty("name") String name,
															@JsonProperty("stopAreaId") String stopAreaId) {
			this.coordinates = coordinates;
			this.isBlocking = isBlocking;
			this.linkId = linkId;
			this.name = name;
			this.stopAreaId = stopAreaId;
		}
	}

	/**
	 * Describes a complete transit line including all its routes.
	 */
	public static class PayloadTransitLine {
		public String id;
		public String name;
		public Map<String, PayloadTransitRoute> transitRoutes;
	}

	/**
	 * Describes a single transit route stop of a transit route.
	 */
	public static class PayloadTransitRouteStop {
		public final String id;
		public final double departureOffset;
		public final double arrivalOffset;
		public final boolean awaitDeparture;

		@JsonCreator
		public PayloadTransitRouteStop(@JsonProperty("id") String id,
																	 @JsonProperty("arrivalOffset") double arrivalOffset,
																	 @JsonProperty("departureOffset") double departureOffset,
																	 @JsonProperty("awaitDeparture") Boolean awaitDeparture) {
			this.id = id;
			this.arrivalOffset = arrivalOffset;
			this.departureOffset = departureOffset;
			this.awaitDeparture = awaitDeparture != null && awaitDeparture;
		}

		@Deprecated(since = "2024/04/20")
		public PayloadTransitRouteStop(String id,
																	 double arrivalOffset,
																	 double departureOffset) {
			this.id = id;
			this.arrivalOffset = arrivalOffset;
			this.departureOffset = departureOffset;
			this.awaitDeparture = false;
		}
	}

	/**
	 * Describes a single departure of a transit route.
	 */
	public static class PayloadTransitDeparture {
		public final String id;
		public final double departureTime;
		public final String vehicleRefId;
		public final String vehicleType;

		@JsonCreator
		public PayloadTransitDeparture(@JsonProperty("id") String id, @JsonProperty("departureTime") double departureTime,
																	 @JsonProperty("vehicleRefId") String vehicleRefId, @JsonProperty("vehicleType") String vehicleType) {
			this.id = id;
			this.departureTime = departureTime;
			this.vehicleRefId = vehicleRefId;
			this.vehicleType = vehicleType;
		}
	}

	/**
	 * Describes a transit route of a transit line.
	 */
	public static class PayloadTransitRoute {
		public final String id;
		public final String mode;
		public final String name;
		public final PayloadTransitRouteStop[] stops;
		public final String[] linkIds;
		public final String transitLineId;
		public final PayloadTransitDeparture[] departures;

		@JsonCreator
		public PayloadTransitRoute(@JsonProperty("transitLineId") String transitLineId, @JsonProperty("id") String id,
															 @JsonProperty("mode") String mode, @JsonProperty("name") String name,
															 @JsonProperty("stops") PayloadTransitRouteStop[] stops,
															 @JsonProperty("links") String[] links,
															 @JsonProperty("departures") PayloadTransitDeparture[] departures) {
			this.id = id;
			this.mode = mode;
			this.name = name;
			this.stops = stops;
			this.linkIds = links;
			this.transitLineId = transitLineId;
			this.departures = departures;
		}
	}

	/**
	 * Create and add a new transit stop to the schedule.
	 */
	public static class AddTransitStopPayload extends ChangePayload {
		public static final String TYPE = "AddTransitStop";

		public final String id;
		public final PayloadTransitStop stop;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public AddTransitStopPayload(@JsonProperty("id") String id, @JsonProperty("stop") PayloadTransitStop stop) {
			this.id = id;
			this.stop = stop;
		}
	}

	/**
	 * Sets a new name and id to an existing transit stop.
	 */
	public static class UpdateTransitStopPayload extends ChangePayload {
		public static final String TYPE = "UpdateTransitStop";

		public final String id;
		public final String newId;
		public final String newName;
		public final String newLinkId;
		public final double[] newCoord;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public UpdateTransitStopPayload(@JsonProperty("id") String id, @JsonProperty("newId") String newId,
																		@JsonProperty("newLinkId") String newLinkId,
																		@JsonProperty("newName") String newName, @JsonProperty("newCoord") double[] newCoord) {
			this.id = id;
			this.newId = newId;
			this.newLinkId = newLinkId;
			this.newName = newName;
			this.newCoord = newCoord;
		}
	}

	/**
	 * Delete a transit stop facility from the schedule.
	 * <p>
	 * If a transit route is still using this stop facility, an exception might happen.
	 */
	public static class DeleteTransitStopPayload extends ChangePayload {
		public static final String TYPE = "DeleteTransitStop";

		public final String id;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public DeleteTransitStopPayload(@JsonProperty("id") String id) {
			this.id = id;
		}
	}

	/**
	 * Group for transit stop modifications.
	 * <p>
	 * Useful e.g. when moving multiple stops at the same time.
	 */
	public static class TransitStopGroupPayload extends ChangePayload {
		public static final String TYPE = "TransitStopGroup";
		public final Change[] modifications;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public TransitStopGroupPayload(@JsonProperty("modifications") Change... modifications) {
			this.modifications = modifications;
		}
	}

	/**
	 * Create a new transit line and add it to the schedule.
	 */
	public static class AddTransitLinePayload extends ChangePayload {
		public static final String TYPE = "AddTransitLine";

		public PayloadTransitLine line;

		@Override
		public String getType() {
			return TYPE;
		}
	}

	/**
	 * Updates the name and Id of a transit line.
	 */
	public static class UpdateTransitLinePayload extends ChangePayload {
		public static final String TYPE = "UpdateTransitLine";

		public final String lineId;
		public final String newLineId;
		public final String newName;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public UpdateTransitLinePayload(@JsonProperty("lineId") String lineId, @JsonProperty("newLineId") String newLineId,
																		@JsonProperty("newName") String newName) {
			this.lineId = lineId;
			this.newLineId = newLineId;
			this.newName = newName;
		}
	}

	/**
	 * Delete a transit line completely including all its transit routes.
	 */
	public static class DeleteTransitLinePayload extends ChangePayload {
		public static final String TYPE = "DeleteTransitLine";

		public String lineId;

		@Override
		public String getType() {
			return TYPE;
		}
	}

	/**
	 * Add a new transit route to an existing line.
	 */
	public static class AddTransitRoutePayload extends ChangePayload {
		public static final String TYPE = "AddTransitRoute";

		public final String lineId;
		public final String routeId;
		public final PayloadTransitRoute route;

		@JsonCreator
		public AddTransitRoutePayload(@JsonProperty("lineId") String lineId, @JsonProperty("routeId") String routeId,
																	@JsonProperty("route") PayloadTransitRoute route) {
			this.lineId = lineId;
			this.routeId = routeId;
			this.route = route;
		}

		@Override
		public String getType() {
			return TYPE;
		}
	}

	/**
	 * Update a specific transit route, essentially replacing it with the specified new route.
	 */
	public static class UpdateTransitRoutePayload extends ChangePayload {
		public static final String TYPE = "UpdateTransitRoute";

		public final String lineId;
		public final String routeId;
		public final PayloadTransitRoute route;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public UpdateTransitRoutePayload(@JsonProperty("lineId") String lineId, @JsonProperty("routeId") String routeId,
																		 @JsonProperty("route") PayloadTransitRoute route) {
			this.lineId = lineId;
			this.routeId = routeId;
			this.route = route;
		}
	}

	/**
	 * Delete a complete transit route from a transit line.
	 */
	public static class DeleteTransitRoutePayload extends ChangePayload {
		public static final String TYPE = "DeleteTransitRoute";

		public final String lineId;
		public final String routeId;

		@Override
		public String getType() {
			return TYPE;
		}

		@JsonCreator
		public DeleteTransitRoutePayload(@JsonProperty("lineId") String lineId, @JsonProperty("routeId") String routeId) {
			this.lineId = lineId;
			this.routeId = routeId;
		}
	}

	/**
	 * Describes that a new Transit route stop must be added to an existing transit route.
	 */
	public static class AddTransitRouteStopPayload extends ChangePayload {
		public static final String TYPE = "AddTransitRouteStop";

		public final String lineId;
		public final String routeId;
		public final String stopId;
		public final String insertAfterStopId;
		public final Integer insertAfterOccurrence;

		@Override
		public String getType() {
			return TYPE;
		}


		@JsonCreator
		public AddTransitRouteStopPayload(@JsonProperty("lineId") String lineId,
																			@JsonProperty("routeId") String routeId,
																			@JsonProperty("stopId") String stopId,
																			@JsonProperty("insertAfterStopId") String insertAfterStopId,
																			@JsonProperty("insertAfterOccurrence") Integer insertAfterOccurrence) {
			this.lineId = lineId;
			this.routeId = routeId;
			this.stopId = stopId;
			this.insertAfterStopId = insertAfterStopId;
			this.insertAfterOccurrence = insertAfterOccurrence;
		}
	}

	/**
	 * Describes that a transit route stop within a specific transit route is modified.
	 */
	public static class UpdateTransitRouteStopPayload extends ChangePayload {
		public static final String TYPE = "UpdateTransitRouteStop";

		public String lineId;
		public String routeId;
		public PayloadTransitRouteStop stop;
		public Integer occurrence;

		@Override
		public String getType() {
			return TYPE;
		}

		public UpdateTransitRouteStopPayload() {
		}

		public UpdateTransitRouteStopPayload(String lineId, String routeId, PayloadTransitRouteStop stop, Integer occurrence) {
			this.lineId = lineId;
			this.routeId = routeId;
			this.stop = stop;
			this.occurrence = occurrence;
		}
	}

	/**
	 * Describes a specific transit route stop within a transit route that should be deleted.
	 * If a transit stop is served multiple times by a route, the occurrence (1-based) specifies
	 * which one of the multiple stops should be deleted.
	 */
	public static class DeleteTransitRouteStopPayload extends ChangePayload {
		public static final String TYPE = "DeleteTransitRouteStop";

		public String lineId;
		public String routeId;
		public String stopId;
		public Integer occurrence;

		@Override
		public String getType() {
			return TYPE;
		}

		public DeleteTransitRouteStopPayload() {
		}

		public DeleteTransitRouteStopPayload(String lineId, String routeId, String stopId, Integer occurrence) {
			this.lineId = lineId;
			this.routeId = routeId;
			this.stopId = stopId;
			this.occurrence = occurrence;
		}
	}

	/**
	 * Lists all departures of a transit route, which should replace all existing departures of this route.
	 */
	public static class UpdateTransitRouteDeparturesPayload extends ChangePayload {
		public static final String TYPE = "UpdateTransitRouteDepartures";

		public final String lineId;
		public final String routeId;
		public final PayloadTransitDeparture[] departures;


		@JsonCreator
		public UpdateTransitRouteDeparturesPayload(@JsonProperty("lineId") String lineId,
																							 @JsonProperty("routeId") String routeId,
																							 @JsonProperty("departures") PayloadTransitDeparture[] departures) {
			this.lineId = lineId;
			this.routeId = routeId;
			this.departures = departures;
		}

		@Override
		public String getType() {
			return TYPE;
		}
	}

	/**
	 * Specifies that within a transit network route, a link should be removed.
	 * If the link is present multiple times (e.g. route loops), then the occurrence specifies which link to delete
	 */
	public static class DeleteTransitNetworkRouteLinkPayload extends ChangePayload {
		public static final String TYPE = "DeleteTransitNetworkRouteLink";

		public String lineId;
		public String routeId;
		public String linkId;
		public Integer occurrence;

		@JsonCreator
		public DeleteTransitNetworkRouteLinkPayload(@JsonProperty("lineId") String lineId,
																								@JsonProperty("routeId") String routeId,
																								@JsonProperty("linkId") String linkId,
																								@JsonProperty("occurrence") Integer occurrence) {
			this.lineId = lineId;
			this.routeId = routeId;
			this.linkId = linkId;
			this.occurrence = occurrence;
		}

		@Override
		public String getType() {
			return TYPE;
		}
	}

	/**
	 * Specifies that within a transit network route, a link should be added.
	 * before tells us if we prepend or append (needed to extend route at the front and back)
	 * If the link we want to insert after/before is present multiple times (e.g. route loops), then the occurrence specifies which link to use
	 * newLinkId is the id of the link to be inserted
	 */
	public static class AddTransitNetworkRouteLinkPayload extends ChangePayload {
		public static final String TYPE = "AddTransitNetworkRouteLink";

		public String lineId;
		public String routeId;
		public String linkId;
		public Integer occurrence;
		public String newLinkId;
		public Boolean before;

		@JsonCreator
		public AddTransitNetworkRouteLinkPayload(@JsonProperty("lineId") String lineId,
																						 @JsonProperty("routeId") String routeId,
																						 @JsonProperty("linkId") String linkId,
																						 @JsonProperty("occurrence") Integer occurrence,
																						 @JsonProperty("newLinkId") String newLinkId,
																						 @JsonProperty("before") Boolean before
																						 ) {
			this.lineId = lineId;
			this.routeId = routeId;
			this.linkId = linkId;
			this.occurrence = occurrence;
			this.newLinkId = newLinkId;
			this.before = before;
		}

		@Override
		public String getType() {
			return TYPE;
		}
	}

}
