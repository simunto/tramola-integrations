package com.simunto.tramola.matsim;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.population.PopulationUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mrieser / Simunto
 */
class TramolaMainModeIdentifierTest {

	@Test
	public void testMainModeIdentifier() {
		TramolaMainModeIdentifier identifier = new TramolaMainModeIdentifier();

		Assertions.assertEquals("car", identifier.identifyMainMode(asLegs("car")));
		Assertions.assertEquals("car", identifier.identifyMainMode(asLegs("walk", "car", "walk")));
		Assertions.assertEquals("car", identifier.identifyMainMode(asLegs("walk", "pt", "car", "walk")));
		Assertions.assertEquals("pt", identifier.identifyMainMode(asLegs("walk", "pt", "drt", "walk")));
		Assertions.assertEquals("drt", identifier.identifyMainMode(asLegs("walk", "drt", "walk")));
		Assertions.assertEquals("walk", identifier.identifyMainMode(asLegs("walk")));
		Assertions.assertEquals("trotti", identifier.identifyMainMode(asLegs("trotti")));
		Assertions.assertEquals("trotti", identifier.identifyMainMode(asLegs("walk", "trotti", "walk")));
		Assertions.assertEquals("bikeSharing", identifier.identifyMainMode(asLegs("walk", "bikeSharing", "walk")));
		Assertions.assertEquals("car", identifier.identifyMainMode(asLegs("walk", "car", "bikeSharing", "walk")));
		Assertions.assertEquals("unknown", identifier.identifyMainMode(asLegs()));
	}

	private List<PlanElement> asLegs(String... modes) {
		List<PlanElement> legs = new ArrayList<>();
		for (String mode : modes) {
			Leg leg = PopulationUtils.createLeg(mode);
			legs.add(leg);
		}
		return legs;
	}
}