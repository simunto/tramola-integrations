package com.simunto.tramola.matsim;

import io.javalin.Javalin;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.matsim.api.core.v01.Scenario;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.controler.OutputDirectoryHierarchy.OverwriteFileSetting;
import org.matsim.core.scenario.ScenarioUtils;

public class TramolaIntegrationTest {

	@Test
	public void testControllerIntegration() {

		// start webserver for mocking
		var mockServer = Javalin.create(config -> {
		});
		int[] eventCounter = {0};
		mockServer.before(ctx -> {
			System.out.println(ctx.method() + " " +ctx.fullUrl());
		});
		mockServer.post("api/v1/datasets/datasets", ctx -> {
			ctx.contentType("application/json");
			ctx.result("{\"id\": \"1\"}");
		});
		mockServer.post("api/v1/datasets/datasets/1/events", ctx -> {
			ctx.contentType("application/json");
			ctx.result("{\"id\": \"1\"}");
			eventCounter[0]++;
		});
		mockServer.post("api/v1/datasets/datasets/1/tables", ctx -> {
			ctx.contentType("application/json");
			ctx.result("{\"id\": \"1\", \"name\": \"test\"}");
		});
		mockServer.post("api/v1/datasets/files", ctx -> {
			ctx.contentType("application/json");
			ctx.result("{\"files\": [{\"id\": \"1\"}] }");
		});
		mockServer.start();
		int port = mockServer.port();


		TramolaIntegrationConfigGroup tramolaConfig = new TramolaIntegrationConfigGroup();
		tramolaConfig.setUrl("http://localhost:" + port);
		tramolaConfig.setApiKey("ABCD-EFGH-IJKL-MNOP");
		tramolaConfig.setEnabled(true);

		Config config = ConfigUtils.createConfig(tramolaConfig);
		config.controller().setLastIteration(1);

		TramolaMatsimIntegration tramola = new TramolaMatsimIntegration(config);
		tramola.init();

		config.controller().setOverwriteFileSetting( OverwriteFileSetting.deleteDirectoryIfExists );

		Scenario scenario = ScenarioUtils.loadScenario(config) ;
		Controler controler = new Controler( scenario ) ;
		controler.addOverridingModule(tramola);
		controler.run();

		mockServer.stop();
		Assertions.assertEquals(2, eventCounter[0], "expected a startup and shutdown event.");
	}

}
