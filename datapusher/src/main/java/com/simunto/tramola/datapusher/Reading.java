package com.simunto.tramola.datapusher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author mrieser
 */
public class Reading {

	private final long timestamp;
	private final Integer iteration;
	private final List<Attribute> values = new ArrayList<>(5);
	private Serie series;

	private Reading(long timestamp, Integer iteration, List<Attribute> attributes) {
		this.timestamp = timestamp;
		this.iteration = iteration;
		this.values.addAll(attributes);
	}

	public static Reading.ReadingBuilder build() {
		return new Reading.ReadingBuilder();
	}

	public long getTimestamp() {
		return this.timestamp;
	}

	public Integer getIteration() {
		return this.iteration;
	}

	public List<Attribute> getValues() {
		return Collections.unmodifiableList(this.values);
	}

	void setSeries(Serie series) {
		this.series = series;
	}

	Serie getSeries() {
		return this.series;
	}

	public static class ReadingBuilder {
		private long timestamp = Timer.currentTimeMillis();
		private Integer iteration = null;
		private List<Attribute> values = new ArrayList<>(5);

		public Reading.ReadingBuilder iteration(int iteration) {
			this.iteration = iteration;
			return this;
		}

		public Reading.ReadingBuilder value(String name, double value) {
			this.values.add(new DoubleAttribute(name, value));
			return this;
		}

		public Reading.ReadingBuilder value(String name, long value) {
			this.values.add(new LongAttribute(name, value));
			return this;
		}

		public Reading.ReadingBuilder value(String name, int value) {
			this.values.add(new IntAttribute(name, value));
			return this;
		}

		public Reading.ReadingBuilder value(String name, boolean value) {
			this.values.add(new BooleanAttribute(name, value));
			return this;
		}

		public Reading.ReadingBuilder value(String name, String value) {
			this.values.add(new StringAttribute(name, value));
			return this;
		}

		public Reading get() {
			return new Reading(this.timestamp, this.iteration, this.values);
		}
	}

}
